#ifndef IGAMEENTITY_HPP
#define IGAMEENTITY_HPP
//#include <ncurses.h>
#include <ncursesw/curses.h>

class IGameEntity
{
	public:
		virtual ~IGameEntity() {}
		virtual void draw(WINDOW *window) = 0;
};

#endif

