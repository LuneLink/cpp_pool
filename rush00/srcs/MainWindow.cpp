#include "MainWindow.hpp"

MainWindow::MainWindow(): gameOver(false) {
	nWindow = subwin(stdscr, 21, 80, 0, 0);
	for(int i = 0; i < 20; i++) {
		_stars[i] = new char[81];
		_stars[i][80] = 0;
	}

}

MainWindow::~MainWindow() {

}

MainWindow::MainWindow(MainWindow const &src) {
	(void)src;
}

MainWindow	&MainWindow::operator=(MainWindow const &src){
	(void)src;
	return (*this);
};

void MainWindow::draw() {
		werase(nWindow);
		box(nWindow, 0, 0);
		drawStars();
		if (_player.checkCollision(nWindow))
		{
			_stars[_player._y][_player._x] = ' ';
			if (_player._hp == 0){
				gameOver = true;
			}
		}
		_player.draw(nWindow);
		wrefresh(nWindow);
}


//---------------------------------Private----------------------------

char		*MainWindow::genSpaceRow(){
	char *stars = new char[81];

	stars[80] = 0;
	for (int i = 0 ; i < 81; i++) {
		int numb = std::rand() % 40;
		if (numb == 1)
			stars[i] = '.';
		else
			stars[i] = ' ';

		int numb2 = std::rand() % 1000;

		if (numb2 == 1)
			stars[i] = 'o';
		else if (numb2 == 9)
			stars[i] = 'v';
		else if (numb2 == 20)
			stars[i] = '+';
		else if (numb2 == 17)
			stars[i] = '=';


	}
	return stars;
}

void MainWindow::drawStars()
{
	delete [] _stars[19];
	for(int i = 19; i >= 0; i--) {
		_stars[i] = _stars[i - 1];
	}
	_stars[0] = genSpaceRow();
	for(int i = 2; i < 19; i++)
	{
		for(int j = 1; j < 79; j++)
		{
			if (_stars[i][j] == 111 || _stars[i][j] == 118)
				wattron(nWindow, COLOR_PAIR(2));
			mvwprintw(nWindow, i, j, "%c",_stars[i][j]);
			if (_stars[i][j] == 111 || _stars[i][j] == 118)
				wattroff(nWindow, COLOR_PAIR(2));
		}

	}

}