#include "AWindow.hpp"

AWindow::AWindow() {
}

AWindow::AWindow(WINDOW *existWindow) {
	this->nWindow = existWindow;
	for (int i = 0; i < 4; i++)
		subWindows[i] = 0;
}

AWindow::AWindow(AWindow const &src){
	this->nWindow = dupwin(src.nWindow);
	for (int i = 0; i < 4; i++) {
		subWindows[i] = new AWindow(*(src.subWindows[i]));
	}
}

AWindow	&AWindow::operator=(AWindow const &src) {


	(void)src;
	return (*this);
}

AWindow::~AWindow() {
	delwin(nWindow);
}

void AWindow::draw() {

}