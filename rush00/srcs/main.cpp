#include <ncurses.h>
#include "Game.hpp"

int main()
{
	initscr();
	curs_set(0);
	Game game;
	game.run();
	endwin();

	return 0;
}

