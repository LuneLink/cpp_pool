#include "InfoWindow.hpp"

InfoWindow::InfoWindow(): _hp(0), _ammo(0), _kills(0){
	nWindow = subwin(stdscr, 3, 80, 21, 0);
}

InfoWindow::~InfoWindow() {

}

InfoWindow::InfoWindow(InfoWindow const &src) {
	(void)src;
}

InfoWindow	&InfoWindow::operator=(InfoWindow const &src){
	(void)src;
	return (*this);
};

void InfoWindow::draw() {
	werase(nWindow);
	box(nWindow, 0, 0);
	mvwprintw(nWindow, 1, 2, "HP: %02d", _hp);
	mvwvline(nWindow, 1, 9, 0 ,1);
	mvwprintw(nWindow, 1, 11, "AMMO: %02d", _ammo);
	mvwvline(nWindow, 1, 20, 0 ,1);
	mvwprintw(nWindow, 1, 22, "KILLS: %02d", _kills);
	wrefresh(nWindow);
}