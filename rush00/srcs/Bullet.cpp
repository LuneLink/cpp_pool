#include "Bullet.hpp"

Bullet::Bullet(int x, int y): _speed(0.4),_isBang(false), _resource(0){
	this->_x = x;
	this->_y = y;
}

Bullet::~Bullet(){

}

Bullet::Bullet(Bullet const &src){
	(void)src;
}

void Bullet::draw(WINDOW *window){
	if (checkCollision(window)) {
		_isBang = true;
	}else{
		mvwprintw(window, (int)_y, _x, "|");
	}
	_y -= _speed;
}

char Bullet::isBang() {
	return _resource;
}

bool Bullet::checkCollision(WINDOW *window) {

	if (_y <= 2)
		return true;

	chtype ch = mvwinch(window, (int)_y - 1, _x);
	char gch = (char)(ch & A_CHARTEXT);
	if (gch != ' ' && gch != '.' && gch != '|') {
		//printf("%c %d %d", gch, );
		_resource = gch;
		mvwprintw(window, (int) _y, _x, " ");
		return true;
	}
	return false;
}