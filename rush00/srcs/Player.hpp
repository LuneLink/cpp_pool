#ifndef CPP_POOL_PLAYER_HPP
#define CPP_POOL_PLAYER_HPP

#include "IGameEntity.hpp"
#include "Bullet.hpp"

class Player :virtual IGameEntity{
	public:
		Player();
		~Player();
		Player(Player const &src);
		void shot();
		void draw(WINDOW *window);
		char checkCollision(WINDOW *window);


	public:
		int		_hp;
		int		_ammo;
		int		_kills;
		int		_x;
		int 	_y;
	private:

		Bullet	*_bullets[30];
		char	_symbol;
};


#endif
