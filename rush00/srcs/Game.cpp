#include "Game.hpp"

Game::Game(): _isRun(true), _updateFrequency(100000){

}

void Game::risui()
{
	WINDOW		*nWindow;

	nWindow = subwin(stdscr, 35, 90, 0, 0);
	werase(nWindow);
	box(nWindow, 0, 0);
	wattron(nWindow, COLOR_PAIR(2));
	mvwprintw(nWindow, 2, 7, "                                                         */////////**");
	mvwprintw(nWindow, 3, 7, "                                                    ,//,/((///((./** ");
	mvwprintw(nWindow, 4, 7, "                               ..,,,*,,,.        /*(//////////(*/*   ");
	mvwprintw(nWindow, 5, 7, "                           ,,**//////////**,,    **,.....,*//(,*,    ");
	mvwprintw(nWindow, 6, 7, "                        ,**///(((((((((((////*,.       ..*//*/,      ");
	mvwprintw(nWindow, 7, 7, "                      ,*//(((((((##((((((((/////,     .*/(*/,        ");
	mvwprintw(nWindow, 8, 7, "                    ,//((((((###########(((((((//*  .,/(,*.          ");
	mvwprintw(nWindow, 9, 7, "                   *//((((##############(((##(((/,.*/(**             ");
	mvwprintw(nWindow, 10, 7, "                  ,//((##############(###%####(*./(//,*))))              ");
	mvwprintw(nWindow, 11, 7, "                  /(((############(###/#/#/###(,,/((/*                 ");
	mvwprintw(nWindow, 12, 7, "                 */(((############/#/##/#/#(,*/((/(//*                 ");
	mvwprintw(nWindow, 13, 7, "                 */(((#####(###/#/##/#/#(**/((/((((//,                 ");
	mvwprintw(nWindow, 14, 7, "               ,/*/(((((###/#/#/##/#(,*/((/(###(((/*                  ");
	mvwprintw(nWindow, 15, 7, "             **//*/(((##/#/#/#/##*.//((/######(((//                   ");
	mvwprintw(nWindow, 16, 7, "          ,/*//,.  /((######/.,//(((#########((/*                    ");
	mvwprintw(nWindow, 17, 7, "        *////*.     /((/*.,//((/(#########(((//.                     ");
	mvwprintw(nWindow, 18, 7, "      *////*...     ..*///((/###########(((/*.                       ");
	mvwprintw(nWindow, 19, 7, "    */,///**....,/////*/*/((((((((((((((/*.                          ");
	mvwprintw(nWindow, 20, 7, "  ,**///////////(,//,       .*//////,.                               ");
	mvwprintw(nWindow, 21, 7, " **/.((((((*,//*                                                     ");
	mvwprintw(nWindow, 22, 7, ",**//////**.                                                         ");

	mvwprintw(nWindow, 25, 12, "  _____ ______ _______   _____  ______          _______     ___ ");
	mvwprintw(nWindow, 26, 12, " / ____|  ____|__   __| |  __ \\|  ____|   /\\   |  __ \\ \\   / / |");
	mvwprintw(nWindow, 27, 12, "| |  __| |__     | |    | |__) | |__     /  \\  | |  | \\ \\_/ /| |");
	mvwprintw(nWindow, 28, 12, "| | |_ |  __|    | |    |  _  /|  __|   / /\\ \\ | |  | |\\   / | |");
	mvwprintw(nWindow, 29, 12, "| |__| | |____   | |    | | \\ \\| |____ / ____ \\| |__| | | |  |_|");
	mvwprintw(nWindow, 30, 12, " \\_____|______|  |_|    |_|  \\_\\______/_/    \\_\\_____/  |_|  (_)   ");

	wattroff(nWindow, COLOR_PAIR(2));
	wrefresh(nWindow);
	napms(3000);
	// window->draw();
}

void		Game::_keyhook(Player * player)
{
	int ch;

	//halfdelay(2);
	if(!(ch = getch()))
		ch = getch();

	switch ( ch )
	{
		case KEY_UP: {
			if (player->_y != 1)
				player->_y -= 1;
			break;
		}
		case KEY_RIGHT: {
			if (player->_x != 77)
				player->_x += 1;
			break;
		}
		case KEY_LEFT: {
			if (player->_x != 1)
				player->_x -= 1;
			break;
		}
		case KEY_DOWN: {
			if (player->_y != 19)
				player->_y += 1;
			break;
		}
		case ' ':
			player->shot();
			break ;
		default:
			break ;
	}
}

void Game::run() {

	struct timeval stop, start;

	gettimeofday(&start, NULL);



	keypad(stdscr, true);
	nodelay(stdscr, true);
	cbreak();
	noecho();
	start_color();
	init_pair(1, COLOR_CYAN, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);

	init_pair(4, COLOR_RED, COLOR_MAGENTA);

	risui();

	MainWindow *window = new MainWindow();
	InfoWindow	*infoWindow = new InfoWindow();





	while (_isRun) {
		if (window->gameOver)
		{
			werase(window->nWindow);
			box(window->nWindow, 0, 0);
			mvwprintw(window->nWindow, 10, 35, "GAME OVER!");
			wrefresh(window->nWindow);
			refresh();
			napms(3000);
			return;
		}
		gettimeofday(&stop, NULL);
		_keyhook(&(window->_player));
		if ((unsigned long)stop.tv_usec - (unsigned long)start.tv_usec > _updateFrequency) {
			erase();
			window->draw();
			infoWindow->_hp = window->_player._hp;
			infoWindow->_ammo = window->_player._ammo;
			infoWindow->_kills = window->_player._kills;
			infoWindow->draw();
			refresh();
			start = stop;
		}

	}

}