#ifndef CPP_POOL_MAINWINDOW_HPP
#define CPP_POOL_MAINWINDOW_HPP

#include "AWindow.hpp"
#include "Player.hpp"

#include "Bullet.hpp"
#include <cstdlib>

class MainWindow: virtual public AWindow{

	public:
		MainWindow();
		~MainWindow();
		MainWindow(MainWindow const &src);

		MainWindow	&operator=(MainWindow const &src);
		void		draw();

		Player			_player;
		bool			gameOver;
	private:
		void		drawStars();
		char		*genSpaceRow();

	private:
		char			*_stars[20];
};


#endif
