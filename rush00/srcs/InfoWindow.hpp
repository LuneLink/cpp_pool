#ifndef CPP_POOL_INFOWINDOW_HPP
#define CPP_POOL_INFOWINDOW_HPP

#include "AWindow.hpp"

class InfoWindow: public virtual AWindow{
	public:
		InfoWindow();
		~InfoWindow();
		InfoWindow(InfoWindow const &src);

		InfoWindow	&operator=(InfoWindow const &src);
		void		draw();

	public:
		unsigned 		_hp;
		unsigned 		_ammo;
		unsigned 		_kills;
};


#endif
