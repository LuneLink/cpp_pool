#ifndef CPP_POOL_AWINDOW_HPP
#define CPP_POOL_AWINDOW_HPP

#include "IGameEntity.hpp"
#include <ncurses.h>

class AWindow{

	public:
		AWindow();
		AWindow(WINDOW *existWindow);
		~AWindow();
		AWindow(AWindow const &src);

		AWindow	&operator=(AWindow const &src);

		virtual void draw();

		WINDOW		*nWindow;
	protected:
		AWindow		*subWindows[4];


};


#endif
