#ifndef CPP_POOL_BULLET_HPP
#define CPP_POOL_BULLET_HPP

#include "IGameEntity.hpp"

class Bullet: IGameEntity{
	public:
		Bullet(int x, int y);
		~Bullet();
		Bullet(Bullet const &src);
		void	draw(WINDOW *window);
		bool	checkCollision(WINDOW *window);
		char 	isBang();
	private:
		int		_x;
		float 	_y;
		float	_speed;
		bool	_isBang;
		char	_resource;
};


#endif
