#include "Player.hpp"

Player::Player(): _hp(10), _ammo(10),_x(35), _y(19), _symbol('A') {
	for (int i = 0; i < 30; i ++) {
		_bullets[i] = 0;
	}
}

Player::~Player() {

}

Player::Player(Player const &src) {
	(void)src;
}

void Player::shot(){
	if (_ammo) {
		for (int i = 0; i < 30; i++)
			if (!_bullets[i]) {
				_bullets[i] = new Bullet(_x, _y - 1);
				_ammo--;
				return;
			}
	}
}

void Player::draw(WINDOW *window) {
	for(int i = 0; i < 30; i++)
	{
		if (_bullets[i]) {
			_bullets[i]->draw(window);
			char resource = _bullets[i]->isBang();

			if (resource) {
				if (resource == '+')
					_hp++;
				else if (resource == '=')
					_ammo += 2;
				else
					_kills++;
				delete _bullets[i];
				_bullets[i] = 0;
			}
		}
	}
	wattron(window, COLOR_PAIR(1));
	mvwprintw(window, _y, _x, "%c", _symbol);
	wattroff(window, COLOR_PAIR(1));
}

char Player::checkCollision(WINDOW *window) {

	if (_y <= 2)
		return true;

	chtype ch = mvwinch(window, (int)_y, _x);
	char gch = (char)(ch & A_CHARTEXT);
	if (gch != ' ' && gch != '.') {
		if (gch == '+')
			_hp++;
		else if (gch == '=')
			_ammo++;
		else
			_hp--;
		mvwprintw(window, (int) _y, _x, " ");
		return true;
	}
	return false;
}