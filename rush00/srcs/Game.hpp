#ifndef CPP_POOL_GAME_HPP
#define CPP_POOL_GAME_HPP

#include "IGameEntity.hpp"
#include "AWindow.hpp"
#include "MainWindow.hpp"
#include "InfoWindow.hpp"

#include <sys/time.h>

class Game{

public:
	Game();

	void run();

	private:
		void			risui();
		void			_keyhook(Player * player);
		bool			_isRun;
	unsigned long	_updateFrequency;

};


#endif
