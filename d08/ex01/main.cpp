#include <iostream>
#include <string>
#include <vector>
#include "span.hpp"

int				main()
{
	Span				spanOne(1);

	std::cout << "Push number to spanOne" << std::endl;
	try {
		spanOne.addNumber(42);
		std::cout << "Success" << std::endl;
	}
	catch (std::exception &e) {
		std::cout << "ERROR: " << e.what() << std::endl;
	}

	std::cout << "Push number to spanOne" << std::endl;
	try {
		spanOne.addNumber(42);
	}
	catch (Span::SpanFullException &e) {
		std::cout << "ERROR: " << e.what() << std::endl;
	}

	//----------------------------------------------------------------------

	Span				span(50000);

	span.addNumber(1);
	span.addNumber(2);
	span.addNumber(3);
	span.addNumber(4);

	std::cout << "find Min max" << std::endl;
	std::cout << "Min: " << span.shortestSpan() << std::endl;
	std::cout << "Max: " << span.longestSpan() << std::endl;

	// --------------------------------------------------------------------

	std::vector<int>	source1;

	for (int i = 0; i <= 5000; i++)
		source1.push_back(i);
	span.addNumber(source1.begin(), source1.end());

	std::cout << "With values from 0 to 5000" << std::endl;
	std::cout << "Min: " << span.shortestSpan() << std::endl;
	std::cout << "Max: " << span.longestSpan() << std::endl;

	// --------------------------------------------------------------------------

	std::vector<int>	source2(50000);
	std::cout << "PUSH 50000 to span" << std::endl;
	try {
		span.addNumber(source2.begin(), source2.end());
	}
	catch (Span::SpanFullException &e) {
		std::cout << "ERROR: " << e.what() << std::endl;
	}
	return (0);
}
