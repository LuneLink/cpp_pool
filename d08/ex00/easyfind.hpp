#ifndef EASYFIND_HPP
# define EASYFIND_HPP


template<typename T>
typename T::iterator		easyfind(T &container, int data)
{
	typename T::iterator	it_end = container.end();

	for (typename T::iterator it = container.begin(); it != it_end; it++)
		if (*it == data)
			return (it);
	return  (it_end);
}

#endif
