//
// Created by Serhii Petrenko on 10/11/18.
//

#include <list>
#include <string>
#include <iostream>
#include "easyfind.hpp"

int				main()
{
	std::list<int>::iterator	result;
	std::list<int>				lst;

	lst.push_back(1);
	lst.push_back(7);
	lst.push_back(21);
	lst.push_back(84);

	std::cout << "Search the value 21: ";
	result = easyfind(lst, 100);
	if (result != lst.end())
		std::cout << *result << std::endl;
	else
		std::cout << "Not found" << std::endl;

	return (0);
}