#include <iostream>
#include <iomanip>
#include <ctime>
#include "Account.class.hpp"

int	Account::_nbAccounts = 0;
int	Account::_totalAmount = 0;
int	Account::_totalNbDeposits = 0;
int	Account::_totalNbWithdrawals = 0;

		Account::Account()
{
	Account(0);
}

		Account::Account(int initial_deposit): _amount(initial_deposit),
											   _nbDeposits(0),
											   _accountIndex(Account::_nbAccounts),
											   _nbWithdrawals(0)
{
	Account::_displayTimestamp();
	Account::_nbAccounts++;
	Account::_totalAmount += initial_deposit;
	std::cout << "index:" << this->_accountIndex << ";amount:" << this->_amount << ";created" << std::endl;
}

	Account::~Account()
{
	Account::_displayTimestamp();
	std::cout << "index:" << _accountIndex
			<< ";amount:" << _amount
			<< ";closed" << std::endl;
}

void	Account::_displayTimestamp( void )
{
	static time_t	t;
	static struct tm *conv_time;

	t = time(0);

	conv_time = localtime(&t);
	std::cout << "[" << 1900 + conv_time->tm_year
			  << std::setw(2) << std::setfill('0') << 1 + conv_time->tm_mon
			  << std::setw(2) << std::setfill('0') << conv_time->tm_mday << "_"
			  << std::setw(2) << std::setfill('0') << conv_time->tm_hour
			  << std::setw(2) << std::setfill('0') << conv_time->tm_min
			  << std::setw(2) << std::setfill('0') << conv_time->tm_sec << "] ";
}

void	Account::displayAccountsInfos()
{
	_displayTimestamp();
	std::cout << "accounts:" << _nbAccounts
			  << ";total:" << _totalAmount
			  << ";deposits:" << _totalNbDeposits
			  << ";withdrawals:" << _totalNbWithdrawals << std::endl;
}

void		Account::makeDeposit(int deposit)
{
	_displayTimestamp();
	std::cout << "index:" << _accountIndex
			  << ";p_amount:" << _amount
			  << ";deposit:" << deposit;

	_amount += deposit;
	_nbDeposits++;
	std::cout << ";amount:" << _amount
			  << ";nb_deposits:" << _nbDeposits
			  << std::endl;
}

bool		Account::makeWithdrawal(int withdrawal)
{
	_displayTimestamp();
	std::cout << "index:" << _accountIndex
			  << ";p_amount:" << _amount
			  << ";withdrawal:";

	if (_amount < withdrawal)
	{
		std::cout << "refused" << std::endl;
		return (false);
	}

	_amount -= withdrawal;
	_nbWithdrawals++;
	Account::_totalNbWithdrawals++;
	std::cout << withdrawal
			  << ";amount:" << _amount
			  << ";nb_withdrawals:" << _nbWithdrawals
			  << std::endl;
	return (true);
}

void		Account::displayStatus(void) const
{
	_displayTimestamp();
	std::cout << "index:" << _accountIndex
			  << ";amount:" << _amount
			  << ";deposits:" << _nbDeposits
			  << ";withdrawals:" << _nbWithdrawals
			  << std::endl;
}

int 		Account::getNbAccounts()
{
	return Account::_nbAccounts;
}

int 		Account::getTotalAmount()
{
	return Account::_totalAmount;
}

int 		Account::getNbDeposits()
{
	return Account::_totalNbDeposits;
}

int 		Account::getNbWithdrawals()
{
	return Account::_totalNbWithdrawals;
}

int			Account::checkAmount() const
{
	return _amount;
}
