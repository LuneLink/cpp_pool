#include <iostream>

int main (int argc, char **argv)
{
	if (argc > 1)
	{
		char *p_str;
		for (int i = 1; i < argc; i++)
		{
			p_str = argv[i];
			while (*p_str)
			{
				std::cout << (char)toupper(*p_str);
				p_str++;
			}
		}
	}
	else
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	std::cout << std::endl;
	return (0);
}
