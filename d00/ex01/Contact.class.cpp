#include "Contact.class.hpp"

static void outStrWire(std::string str)
{
	if (str.length() > 10)
	{
		str[9] = '.';
		str = str.substr(0, 10);
	}
	std::cout<<std::setw(10)<<str<<"|";
}

Contact::Contact()
{

}

static bool isleapyear(int year){
	return ((!(year%4) && (year%100)) || !(year%400));
}

static bool valid_date(int year, int month, int day)
{
	unsigned short monthlen[]={31,28,31,30,31,30,31,31,30,31,30,31};
	if (!year || !month || !day || month>12)
		return 0;
	if (isleapyear(year) && month==2)
		monthlen[1]++;
	if (day>monthlen[month-1])
		return 0;
	return 1;
}

void 	Contact::readFirstName()
{
	std::cout << "Enter first name" << std::endl;
	std::cin >> this->first_name;
}

void 	Contact::readLastName()
{
	std::cout << "Enter last name" << std::endl;
	std::cin >> this->last_name;
}

void	Contact::readNickname()
{
	std::cout << "Enter nickname" << std::endl;
	std::cin >> this->nickname;
}

void	Contact::readLogin()
{
	std::cout << "Enter login" << std::endl;
	std::cin >> this->login;
}

void	Contact::readPostalAddress()
{
	std::cout << "Enter postal address" << std::endl;
	std::cin >> this->postal_address;
}

void	Contact::readEmailAddress()
{
	std::cout << "Enter email address" << std::endl;
	std::cin >> this->email_address;
}

void	Contact::readPhoneNumber()
{
	std::cout << "Enter phone number" << std::endl;
	std::cin >> this->phone_number;
}

void	Contact::readBirthdayDate()
{
	int				year;
	int				month;
	int				day;
	bool			work = true;
	std::string		reader;

	while (work)
	{
		std::cout << "Enter Your birthday date" << std::endl;
		std::cout << "Enter year" << std::endl;
		std::cin >> reader;
		year = atoi(reader.c_str());
		std::cout << "Enter month" << std::endl;
		std::cin >> reader;
		month = atoi(reader.c_str());
		std::cout << "Enter day" << std::endl;
		std::cin >> reader;
		day = atoi(reader.c_str());
		if (valid_date(year, month, day))
		{
			birthday_date.tm_year = year;
			birthday_date.tm_mon = month;
			birthday_date.tm_mday = day;
			work = false;
		}
		else
			std::cout << "INVALID DATE! TRY AGAIN!" << std::endl;
	}
}

void Contact::readFavoriteMeal()
{
	std::cout << "Enter favorite meal" << std::endl;
	std::cin >> this->favorite_meal;
}

void Contact::readUnderwearColor()
{
	std::cout << "Enter underwear color" << std::endl;
	std::cin >> this->underwear_color;
}

void Contact::readDarkestSecret()
{
	std::cout << "Enter darkest secret" << std::endl;
	std::cin.get();
	std::getline(std::cin, this->darkest_secret);
}

std::string	Contact::getFirstName() const
{
	return (first_name);
}

std::string Contact::getLastName() const
{
	return (last_name);
}

std::string Contact::getNickname() const
{
	return (nickname);
}

std::string Contact::getLogin()
{
	return (login);
}

std::string Contact::getPostalAddress()
{
	return (postal_address);
}

std::string Contact::getEmailAddress()
{
	return (email_address);
}

size_t Contact::getPhoneNumber()
{
	return (phone_number);
}

std::string Contact::getFavoriteMeal()
{
	return (favorite_meal);
}

std::string Contact::getUnderwearColor()
{
	return (underwear_color);
}

std::string Contact::getDarkestSecret()
{
	return (darkest_secret);
}

void 		Contact::outSomeDetails() const
{
	outStrWire(getFirstName());
	outStrWire(getLastName());
	outStrWire(getNickname());
}

void 		Contact::outFullDetails() const
{
	std::cout << std::setw(17) << "First Name: " << first_name << std::endl;
	std::cout << std::setw(17) << "Last Name: " << last_name << std::endl;
	std::cout << std::setw(17) << "Nickname: " << nickname << std::endl;
	std::cout << std::setw(17) << "Login: " << login << std::endl;
	std::cout << std::setw(17) << "Postal Address: " << postal_address << std::endl;
	std::cout << std::setw(17) << "Email Address: " << email_address << std::endl;
	std::cout << std::setw(17) << "Phone Number: " << phone_number << std::endl;
	std::cout << std::setw(17) << "Birthday Date: ";
	if (birthday_date.tm_mday < 10)
		std::cout << "0";
	std::cout << birthday_date.tm_mday << ".";
	if (birthday_date.tm_mon < 10)
		std::cout << "0";
	std::cout << birthday_date.tm_mon << ".";
	std::cout << birthday_date.tm_year << std::endl;
	std::cout << std::setw(17) << "Favorite Meal: " << first_name << std::endl;
	std::cout << std::setw(17) << "Underwear Color: " << underwear_color << std::endl;
	std::cout << std::setw(17) << "Darkest Secret: " << darkest_secret << std::endl;
}

