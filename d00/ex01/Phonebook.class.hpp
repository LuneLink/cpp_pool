
#ifndef CPP_POOL_PHONEBOOK_H
# define CPP_POOL_PHONEBOOK_H

# include <string>
# include <iostream>
# include <iomanip>
# include <cstdlib>
# include "Contact.class.hpp"

class Phonebook
{
	private:
		Contact contacts[8];
		char 	empty[8];

	private:
		int		is_empty() const;
		void 	add_contact();
		void 	search() const;
		bool	isInContacts(int index) const;

	public:
				Phonebook();
		void	use();
};

#endif
