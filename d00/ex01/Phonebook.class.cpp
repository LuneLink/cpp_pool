
#include "Phonebook.class.hpp"

Phonebook::Phonebook()
{
	for (size_t i = 0; i < 8; i++)
		empty[i] = 0;
}

int Phonebook::is_empty() const
{
	for (int i = 0; i < 8; i++)
		if (empty[i] == 0)
			return (i);
	return (-1);
}

void Phonebook::add_contact()
{
	int			i;
	std::string	str;

	if ((i = is_empty()) != -1)
	{
		Contact *contact = &(contacts[i]);

		contact->readFirstName();
		contact->readLastName();
		contact->readNickname();
		contact->readLogin();
		contact->readPostalAddress();
		contact->readEmailAddress();
		contact->readBirthdayDate();
		contact->readFavoriteMeal();
		contact->readUnderwearColor();
		contact->readDarkestSecret();
		empty[i] = 1;
	}
}

bool Phonebook::isInContacts(int index) const
{
	return empty[index];
}


void Phonebook::search() const
{
	std::cout << std::setw(10) << "Index" << "|";
	std::cout << std::setw(10) << "First name" << "|";
	std::cout << std::setw(10) << "Last name" << "|";
	std::cout << std::setw(10) << "Nickname" << "|" << std::endl;
	for(int i = 0; i < 8; i++)
	{
		if (empty[i])
		{
			std::cout << std::setw(10) << i << "|";
			contacts[i].outSomeDetails();
			std::cout << std::endl;
		}
	}
	bool		run = true;
	std::string val;
	int 		numb;

	while(run)
	{
		std::cout << "Enter index for show full details or \"BACK\" to return to prev menu" << std::endl;
		std::cin >> val;
		if (val.compare("BACK") == 0)
			run = false;
		if (val.compare("0") == 0)
		{
			if (isInContacts(0))
				contacts[0].outFullDetails();
		}
		else
		{
			numb = atoi(val.c_str());
			if (numb > 0)
				contacts[numb].outFullDetails();
		}
	}
}

void Phonebook::use()
{
	std::string	reader;
	bool 		run;

	run = 1;
	while(run)
	{
		std::cout << "Enter next command (ADD, SEARCH, EXIT)" << std::endl;
		std::cin >> reader;
		if (reader.compare("ADD") == 0)
			add_contact();
		else if (reader.compare("SEARCH") == 0)
			search();
		else if (reader.compare("EXIT") == 0)
			run = 0;
	}
}