
#ifndef CONTACT_CLASS_H
# define CONTACT_CLASS_H

# include <string>
# include <iomanip>
# include <ctime>
# include <iostream>
# include <cstdlib>

class Contact
{

private:
	std::string first_name;
	std::string last_name;
	std::string nickname;
	std::string login;
	std::string postal_address;
	std::string email_address;
	size_t		phone_number;
	struct tm	birthday_date;
	std::string favorite_meal;
	std::string underwear_color;
	std::string darkest_secret;

public:
				Contact();

	void 		readFirstName();
	void 		readLastName();
	void 		readNickname();
	void 		readLogin();
	void 		readPostalAddress();
	void		readEmailAddress();
	void 		readPhoneNumber();
	void 		readBirthdayDate();
	void 		readFavoriteMeal();
	void 		readUnderwearColor();
	void 		readDarkestSecret();
	std::string	getFirstName() const;
	std::string getLastName() const;
	std::string getNickname() const;
	std::string getLogin();
	std::string getPostalAddress();
	std::string getEmailAddress();
	size_t		getPhoneNumber();
	std::string getBirthdayDate();
	std::string getFavoriteMeal();
	std::string getUnderwearColor();
	std::string getDarkestSecret();
	void 		outSomeDetails() const;
	void 		outFullDetails() const;

};

#endif