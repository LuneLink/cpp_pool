#include <string>
#include <iostream>

template<typename T>
void			iter(T *array, size_t len, void (*func)(T &))
{
	for (size_t i = 0;i < len; i++)
		(*func)(array[i]);
}

template<typename T>
void			print(T &data)
{
	std::cout << data << std::endl;
}

template<typename T>
void			print2(T &data)
{
	std::cout << "| " << data << " |"<< std::endl;
}

int main()
{
	int				array_ints[] = {
			1, 2, 3, 4, 5
	};

	std::string		array_strings[] = {
			"str1", "str2", "str3", "str4", "str5"
	};

	iter<int>(array_ints, 5, &print<int>);
	iter<std::string>(array_strings, 5, &print<std::string>);
	iter<int>(array_ints, 5, &print2<int>);
	iter<std::string>(array_strings, 5, &print2<std::string>);

	return (0);
}
