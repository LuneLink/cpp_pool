#ifndef ARRAY_HPP
# define ARRAY_HPP

# include <exception>

template <typename T>
class Array {

	private:
		unsigned int			_size;
		T						*_datas;


	public:
		class					OutOfRangeException: public std::exception
		{
		public:
			OutOfRangeException() {}
			~OutOfRangeException() throw() {}
			char const				*what() const throw() { return ("Out of range");}
		};


		Array<T>() {
			_size = 0;
			_datas = 0;
		}

		Array<T>(unsigned int n) {
			_size = n;
			_datas = new T[_size]();

		}

		Array<T>(Array<T> const &src)
		{
			this->operator=(src);
		}

		~Array(){
			delete [] _datas;
		}

		size_t					size() const {
			return _size;
		}

		Array<T>				&operator=(Array<T> const &rhs)
		{
			delete (_datas);
			_size = rhs._size;
			_datas = new T[_size];

			for (size_t i = 0; i < _size; i++)
				_datas[i] = rhs._datas[i];
			return (*this);
		}

		T	&operator[](unsigned int i) throw(OutOfRangeException)
		{
			if (!_datas || i >= _size)
				throw (OutOfRangeException());
			return (_datas[i]);
		}
};


#endif
