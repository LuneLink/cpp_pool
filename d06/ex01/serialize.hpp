//
// Created by Serhii Petrenko on 10/10/18.
//

#ifndef SERIALIZE_HPP
# define SERIALIZE_HPP

# include <iostream>
# include <string>
# include <time.h>
# include <stdlib.h>

struct Data
{
	std::string		s1;
	int				n;
	std::string		s2;
};

void		*serialize(void);
Data		*deserialize(void * raw);

#endif
