//
// Created by Serhii Petrenko on 10/10/18.
//

#include "SuperString.hpp"

#include <iostream>
#include <cctype>
#include <iomanip>
#include <cmath>


int getPrecision(char *s) {

	unsigned int i = 0;
	unsigned int j;

	while (s[i] != 0 && s[i] != '.') {
		i++;
	}
	if (strlen(s) == i) {
		return 1;
	}
	j = i + 1;
	while (s[j] != 0 && isdigit(s[j])) {
		j++;
	}
	j -= i;
	if (j == 1) {
		return 1;
	} else {
		return (j - 1);
	}
}

void outConvert(SuperString &str) {
	std::cout << "char: ";
	try {
		char ch = static_cast<char>(str);

		if (isprint(ch)) {
			std::cout << ch << std::endl;
		}
		else
			std::cout << "Non displayable" << std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

	std::cout << "int: ";
	try {
		int integ = static_cast<int>(str);
		std::cout << integ << std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

	std::cout << std::fixed;
	std::cout.precision(1);

	std::cout << "float: ";
	try {
		float fnum = static_cast<float>(str);
		if (fnum > 0 &&  isinf(fnum))
			std::cout  << "+";
		std::cout << fnum << "f" <<  std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

	std::cout << "double: ";
	try {

		double dnum = static_cast<double>(str);
		if (dnum > 0 && isinf(dnum))
			std::cout  << "+";
		std::cout << dnum <<  std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

}

int main(int argc, char **argv) {

	if (argc == 2)
	{
		SuperString sString(argv[1]);
		outConvert(sString);
	}
}