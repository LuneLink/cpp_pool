//
// Created by Serhii Petrenko on 10/10/18.
//

#include <errno.h>
#include "SuperString.hpp"

SuperString::SuperString(std::string const &data)
{
	_str = data;
}

SuperString::SuperString(SuperString const &src)
{
	*this = src;
}

SuperString::~SuperString() {}

SuperString					&SuperString::operator=(SuperString const &rhs) {
	_str = rhs._str;
	return (*this);
}

SuperString::CantConvertException::CantConvertException() {

}

SuperString::CantConvertException::~CantConvertException() throw() {

}

char const	*SuperString::CantConvertException::what() const throw()
{
	return ("impossible");
}


SuperString::operator std::string const &() const{
	return (_str);
}

SuperString::operator char() const{
	char	n = static_cast<char>(std::atoi(this->_str.c_str()));
	if (errno) {
		errno = 0;
		throw (CantConvertException());
	}
	return (n);
}

SuperString::operator int() const{
	int	n = std::atoi(this->_str.c_str());
	if (errno){
		errno = 0;
		throw (CantConvertException());
	}
	return (n);
}

SuperString::operator float() const{
	char *check;
	float	n = static_cast<float>(std::strtod(_str.c_str(), &check));

	std::string checkus(check);
	if (errno || _str.compare(check) == 0){
		errno = 0;
		throw (CantConvertException());
	}
	return (n);
}

SuperString::operator double() const{
	char *check;

	double n = std::strtod(this->_str.c_str(), &check);

	std::string checkus(check);
	if (errno || _str.compare(check) == 0){
		errno = 0;
		throw (CantConvertException());
	}
	return (n);
}