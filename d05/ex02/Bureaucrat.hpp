#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include <iostream>
# include <string>
# include <exception>
# include "Form.hpp"

class Form;

class Bureaucrat
{
	private:
		std::string				_name;
		int						_grade;

	public:
		class					GradeTooHighException: public std::exception
		{
			public:
				GradeTooHighException();
				~GradeTooHighException() throw();
				GradeTooHighException(GradeTooHighException const &src);
				GradeTooHighException	&operator=(GradeTooHighException const &rhs);

				char const				*what() const throw();
		};

		class					GradeTooLowException: public std::exception
		{
			public:
				GradeTooLowException();
				~GradeTooLowException() throw();
				GradeTooLowException(GradeTooLowException const &src);
				GradeTooLowException	&operator=(GradeTooLowException const &rhs);

				char const				*what() const throw();
		};

		Bureaucrat(std::string const &name, int grade);
		~Bureaucrat();
		Bureaucrat(Bureaucrat const &src);
		Bureaucrat				&operator=(Bureaucrat const &rhs);

		void					incrementGrade();
		void					decrementGrade();

		void					signForm(Form &form) const;
		void					executeForm(Form &form) const;

		std::string	const		&getName() const;
		int						getGrade() const;
};

std::ostream					&operator<<(std::ostream &os, Bureaucrat const &bureaucrat);

#endif

