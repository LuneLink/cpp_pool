#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP


# include <string>
# include <iostream>
# include <time.h>
# include <stdlib.h>
# include "Form.hpp"

class RobotomyRequestForm: public Form
{
	private:
		std::string				_target;

	public:
		RobotomyRequestForm(std::string const &target);
		~RobotomyRequestForm();
		RobotomyRequestForm(RobotomyRequestForm const &src);
		RobotomyRequestForm		&operator=(RobotomyRequestForm const &rhs);

		void					beExecuted();

		std::string	const		&getTarget() const;
};

#endif
