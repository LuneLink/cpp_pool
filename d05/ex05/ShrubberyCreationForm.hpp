#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP

# include <string>
# include <iostream>
# include <fstream>
# include "Form.hpp"

class ShrubberyCreationForm: public Form
{
	private:
		std::string				_target;

	public:
								ShrubberyCreationForm(std::string const &target);
								~ShrubberyCreationForm();
								ShrubberyCreationForm(ShrubberyCreationForm const &src);
		ShrubberyCreationForm	&operator=(ShrubberyCreationForm const &rhs);

		void					beExecuted() const;

		std::string	const		&getTarget() const;
};

#endif
