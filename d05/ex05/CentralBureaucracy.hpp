#ifndef CENTRALBUREAUCRACY_HPP
# define CENTRALBUREAUCRACY_HPP

# include <string>
# include <iostream>
# include "OfficeBlock.hpp"

class CentralBureaucracy
{
	private:
		static std::string		_forms[];
		static unsigned			_formsCount;

		OfficeBlock				_offices[20];
		unsigned				_officeCount;
		unsigned				_bureaucratCount;

		std::string				_queue[20];
		unsigned				_queueSize;

	public:
		CentralBureaucracy();
		~CentralBureaucracy();
		CentralBureaucracy(CentralBureaucracy const &src);
		CentralBureaucracy		&operator=(CentralBureaucracy const &rhs);

		void					feed(Bureaucrat &bureaucrat);
		void					queueUp(std::string const &target);
		void					doBureaucracy();
};


#endif
