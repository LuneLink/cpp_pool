#include <exception>
#include <iostream>
#include <string>
#include "Bureaucrat.hpp"
#include "CentralBureaucracy.hpp"

int				main()
{
	CentralBureaucracy	rightSector;

	try
	{
		rightSector.feed(*new Bureaucrat("Professor1", 5));
		rightSector.feed(*new Bureaucrat("Professor2", 3));
		rightSector.feed(*new Bureaucrat("Professor3", 1));
		rightSector.feed(*new Bureaucrat("Professor4", 5));
		rightSector.feed(*new Bureaucrat("Professor5", 3));
		rightSector.feed(*new Bureaucrat("Professor6", 1));
		rightSector.feed(*new Bureaucrat("Professor7", 5));

		rightSector.queueUp("Me");
		rightSector.queueUp("Serhii");
		rightSector.queueUp("Andy");
		rightSector.queueUp("Lex");
		rightSector.queueUp("Luck");
		rightSector.queueUp("Nicolas");
		rightSector.queueUp("Drake");
		rightSector.queueUp("Boss");
		rightSector.queueUp("Polina");
		rightSector.doBureaucracy();
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR : " << e.what() << std::endl;
	}

	return (0);
}

