#include "CentralBureaucracy.hpp"

unsigned					CentralBureaucracy::_formsCount = 3;
std::string					CentralBureaucracy::_forms[3] = {
		"shrubbery creation",
		"robotomy request",
		"presidential pardon"
};

CentralBureaucracy::CentralBureaucracy()
{
	_officeCount = 0;
	_bureaucratCount = 0;
	_queueSize = 0;
	srand(time(NULL));
}

CentralBureaucracy::CentralBureaucracy(CentralBureaucracy const &src)
{
	*this = src;
}

CentralBureaucracy::~CentralBureaucracy()
{

}

CentralBureaucracy	&CentralBureaucracy::operator=(CentralBureaucracy const &rhs)
{
	_officeCount		= rhs._officeCount;
	_bureaucratCount	= rhs._bureaucratCount;
	_queueSize			= rhs._queueSize;


	for (unsigned i = 0; i < _officeCount; i++)
		_offices[i] = rhs._offices[i];
	for (unsigned i = 0; i < _queueSize; i++)
		_queue[i] = rhs._queue[i];
	return (*this);
}

void						CentralBureaucracy::feed(Bureaucrat &bureaucrat)
{
	if (_bureaucratCount < 40)
	{
		if ((_bureaucratCount % 2) == 0)
			_offices[_officeCount].setSigner(bureaucrat);
		else
		{
			_offices[_officeCount].setExecutor(bureaucrat);
			_offices[_officeCount].setIntern(*(new Intern()));
			_officeCount++;
		}
		_bureaucratCount++;
	}
}

void						CentralBureaucracy::queueUp(std::string const &target)
{
	if (_queueSize < 20)
	{
		_queue[_queueSize] = target;
		_queueSize++;
	}
}

void						CentralBureaucracy::doBureaucracy()
{
	for (unsigned i = 0; i < _queueSize; i++)
		_offices[rand() % _officeCount].doBureaucracy(_forms[rand() % _formsCount], _queue[i]);
	_queueSize = 0;
}