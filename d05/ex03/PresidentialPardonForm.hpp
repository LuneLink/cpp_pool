#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP

# include <string>
# include <iostream>
# include "Form.hpp"

class PresidentialPardonForm: public Form
{
	private:
		std::string				_target;

	public:
								PresidentialPardonForm(std::string const &target);
								~PresidentialPardonForm();
								PresidentialPardonForm(PresidentialPardonForm const &src);
		PresidentialPardonForm	&operator=(PresidentialPardonForm const &rhs);

		void					beExecuted() const;

		std::string	const		&getTarget() const;
};

std::ostream&         operator<<(std::ostream& output, PresidentialPardonForm const& rhs);

#endif
