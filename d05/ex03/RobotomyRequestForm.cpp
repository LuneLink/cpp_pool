#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(std::string const &target):
	Form("Robotomy Request Form", 72, 45),
	_target(target)
{
	srand(time(NULL));
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &src):
	Form("Robotomy Request Form", 72, 45)
{
	*this = src;
}

RobotomyRequestForm::~RobotomyRequestForm()
{}

RobotomyRequestForm			&RobotomyRequestForm::operator=(RobotomyRequestForm const &rhs)
{
	this->_target = rhs._target;
	return (*this);
}

void						RobotomyRequestForm::beExecuted() const
{
	if (rand() % 2)
		std::cout << this->_target << " has been robotomized succefully !" << std::endl;
	else
		std::cout << "Oups... something happend during the robotomisation !" << std::endl;
}

std::string const			&RobotomyRequestForm::getTarget() const { return (this->_target); }
