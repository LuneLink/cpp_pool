#include <exception>
#include <iostream>
#include <string>
#include "Bureaucrat.hpp"

int				main()
{
	try
	{
		Bureaucrat	b1("b1", 2);
		std::cout << b1;
		b1.incrementGrade();
		std::cout << b1;
		b1.incrementGrade();
		std::cout << b1;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b1 "<< e.what() << std::endl;
	}

	try
	{
		Bureaucrat	b2("b2", 0);
	}
	catch (Bureaucrat::GradeTooHighException &e)
	{
		std::cout << "ERROR b2 " << e.what() << std::endl;
	}

	return (0);
}