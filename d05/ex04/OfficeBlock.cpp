#include "OfficeBlock.hpp"

OfficeBlock::NoInternException::NoInternException() {}
OfficeBlock::NoInternException::~NoInternException() throw() {}
OfficeBlock::NoInternException::NoInternException(NoInternException const &src)
{
	*this = src;
}

OfficeBlock::NoInternException	&OfficeBlock::NoInternException::operator=(OfficeBlock::NoInternException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*OfficeBlock::NoInternException::what() const throw()
{
	return ("No Intern");
}

OfficeBlock::NoSignerException::NoSignerException() {}
OfficeBlock::NoSignerException::~NoSignerException() throw() {}
OfficeBlock::NoSignerException::NoSignerException(NoSignerException const &src)
{
	*this = src;
}

OfficeBlock::NoSignerException	&OfficeBlock::NoSignerException::operator=(OfficeBlock::NoSignerException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*OfficeBlock::NoSignerException::what() const throw()
{
	return ("No Signer");
}

OfficeBlock::NoExecutorException::NoExecutorException() {}
OfficeBlock::NoExecutorException::~NoExecutorException() throw() {}
OfficeBlock::NoExecutorException::NoExecutorException(NoExecutorException const &src)
{
	*this = src;
}

OfficeBlock::NoExecutorException	&OfficeBlock::NoExecutorException::operator=(OfficeBlock::NoExecutorException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*OfficeBlock::NoExecutorException::what() const throw()
{
	return ("No Executor");
}

OfficeBlock::SignerGradeTooLowException::SignerGradeTooLowException() {}
OfficeBlock::SignerGradeTooLowException::~SignerGradeTooLowException() throw() {}
OfficeBlock::SignerGradeTooLowException::SignerGradeTooLowException(SignerGradeTooLowException const &src)
{
	*this = src;
}

OfficeBlock::SignerGradeTooLowException	&OfficeBlock::SignerGradeTooLowException::operator=(OfficeBlock::SignerGradeTooLowException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*OfficeBlock::SignerGradeTooLowException::what() const throw()
{
	return ("Signer Grade Too Low");
}

OfficeBlock::ExecutorGradeTooLowException::ExecutorGradeTooLowException() {}
OfficeBlock::ExecutorGradeTooLowException::~ExecutorGradeTooLowException() throw() {}
OfficeBlock::ExecutorGradeTooLowException::ExecutorGradeTooLowException(ExecutorGradeTooLowException const &src)
{
	*this = src;
}

OfficeBlock::ExecutorGradeTooLowException	&OfficeBlock::ExecutorGradeTooLowException::operator=(OfficeBlock::ExecutorGradeTooLowException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*OfficeBlock::ExecutorGradeTooLowException::what() const throw()
{
	return ("Executor Grade Too Low");
}


//--------------------------------------------------OFFICEBLOCK------------------------------------------------

OfficeBlock::OfficeBlock() {
	this->_executor = 0;
	this->_signer = 0;
	this->_intern = 0;
}

OfficeBlock::OfficeBlock(Intern &intern, Bureaucrat &signer, Bureaucrat &executor)
{
	this->_intern = &intern;
	this->_signer = &signer;
	this->_executor = &executor;
}

OfficeBlock::~OfficeBlock() {}


void						OfficeBlock::doBureaucracy(std::string const &name, std::string const &target)
{
	Form					*form;

	if (!this->_intern)
		throw (NoInternException());
	if (!this->_signer)
		throw (NoSignerException());
	if (!this->_executor)
		throw (NoExecutorException());

	form = this->_intern->makeForm(name, target);

	if (!form)
		return;

	try
	{
		this->_signer->signForm(*form);
	}
	catch (Form::GradeTooLowException &e)
	{
		throw (SignerGradeTooLowException());
	}

	try
	{
		this->_executor->executeForm(*form);
	}
	catch (Form::GradeTooLowException &e)
	{
		throw (ExecutorGradeTooLowException());
	}
}

void						OfficeBlock::setIntern(Intern &intern) {
	this->_intern = &intern;
}

void						OfficeBlock::setSigner(Bureaucrat &signer) {
	this->_signer = &signer;
}

void						OfficeBlock::setExecutor(Bureaucrat &executor) {
	this->_executor = &executor;
}
