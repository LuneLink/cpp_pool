#include <exception>
#include <iostream>
#include <string>
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"
#include "OfficeBlock.hpp"

void				main() {
	Intern idiotOne;
	Bureaucrat hermes = Bureaucrat("Hermes Conrad", 37);
	Bureaucrat bob = Bureaucrat("Bobby Bobson", 123);
	OfficeBlock ob;
	ob.setIntern(idiotOne);
	ob.setSigner(bob);
	ob.setExecutor(hermes);
	try
	{
		ob.doBureaucracy("mutant pig termination", "Pigley");
	}
	catch (std::exception & e)
	{
		std::cout << "ERROR : " << e.what() << std::endl;
	}

	try
	{
		ob.doBureaucracy("shrubbery creation", "home");
		ob.doBureaucracy("robotomy request", "school");
		ob.doBureaucracy("presidential pardon", "president");
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR : " << e.what() << std::endl;
	}
}


