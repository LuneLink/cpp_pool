#ifndef FORM_HPP
# define FORM_HPP

# include <iostream>
# include <string>
# include <exception>
# include "Bureaucrat.hpp"

class Bureaucrat;

class Form
{
	private:
		std::string				_name;
		bool					_signed;
		int						_minSignGrade;
		int						_minExecGrade;

	public:
		class					GradeTooHighException: public std::exception
		{
			public:
				GradeTooHighException();
				~GradeTooHighException() throw();
				GradeTooHighException(GradeTooHighException const &src);
				GradeTooHighException	&operator=(GradeTooHighException const &rhs);

				char const				*what() const throw();
		};

		class					GradeTooLowException: public std::exception
		{
			public:
				GradeTooLowException();
				~GradeTooLowException() throw();
				GradeTooLowException(GradeTooLowException const &src);
				GradeTooLowException	&operator=(GradeTooLowException const &rhs);

				char const				*what() const throw();
		};

		Form(std::string const &name, int minSignGrade, int minExecGrade)
		throw(GradeTooHighException, GradeTooLowException);
		~Form();
		Form(Form const &src);
		Form					&operator=(Form const &rhs);

		void					beSigned(Bureaucrat const &bureaucrat)
		throw(GradeTooLowException);

		bool					isSigned() const;
		std::string	const		&getName() const;
		int						getMinSignGrade() const;
		int						getMinExecGrade() const;
};

std::ostream					&operator<<(std::ostream &os, Form const &Form);

#endif
