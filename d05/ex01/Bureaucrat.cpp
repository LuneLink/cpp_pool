#include "Bureaucrat.hpp"
#include <iostream>
#include <string>
#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(std::string const &name, int grade):
		_name(name)
{
	if (grade < 1)
		throw (Bureaucrat::GradeTooHighException());
	if (grade > 150)
		throw (Bureaucrat::GradeTooLowException());
	this->_grade = grade;
}

Bureaucrat::Bureaucrat(Bureaucrat const &src)
{
	*this = src;
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat					&Bureaucrat::operator=(Bureaucrat const &rhs)
{
	(void)rhs;
	return (*this);
}

void						Bureaucrat::incrementGrade()
{
	if (this->_grade <= 1)
		throw (Bureaucrat::GradeTooHighException());
	this->_grade--;
}

void						Bureaucrat::decrementGrade()
{
	if (this->_grade >= 150)
		throw (Bureaucrat::GradeTooLowException());
	this->_grade++;
}

std::string	const			&Bureaucrat::getName() const {
	return (this->_name);
}

int							Bureaucrat::getGrade() const {
	return (this->_grade);
}

std::ostream				&operator<<(std::ostream &os, Bureaucrat const &bureaucrat)
{
	os << bureaucrat.getName()
	   << ", bureaucrat grade " << bureaucrat.getGrade() << std::endl;
	return (os);
}

Bureaucrat::GradeTooHighException::GradeTooHighException() {

}

Bureaucrat::GradeTooHighException::~GradeTooHighException() throw() {

}

Bureaucrat::GradeTooHighException::GradeTooHighException(GradeTooHighException const &src)
{
	*this = src;
}

Bureaucrat::GradeTooHighException	&Bureaucrat::GradeTooHighException::operator=(Bureaucrat::GradeTooHighException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*Bureaucrat::GradeTooHighException::what() const throw()
{
	return ("Grade Too High");
}

Bureaucrat::GradeTooLowException::GradeTooLowException() {

}

Bureaucrat::GradeTooLowException::~GradeTooLowException() throw() {

}

Bureaucrat::GradeTooLowException::GradeTooLowException(GradeTooLowException const &src)
{
	*this = src;
}

Bureaucrat::GradeTooLowException	&Bureaucrat::GradeTooLowException::operator=(Bureaucrat::GradeTooLowException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

void						Bureaucrat::signForm(Form &form) const
{
	try
	{
		form.beSigned(*this);
		std::cout << this->_name
				  << " signs " << form.getName()
				  << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << this->_name
				  << " cannot sign " << form.getName()
				  << " because " << e.what()
				  << std::endl;
	}
}

char const					*Bureaucrat::GradeTooLowException::what() const throw()
{
	return ("Grade Too Low");
}