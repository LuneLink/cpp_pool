//
// Created by lunelink on 5/8/18.
//

#include "FragTrap.hpp"

unsigned int FragTrap::_serialNumber = 1;

FragTrap::FragTrap(void): _hitPoints(100),
						  _maxHitPoints(100),
						  _maxEnergyPoints(100),
						  _energyPoints(100),
						  _level(1),
						  _meleeAttackDamage(30),
						  _rangedAttackDamage(20),
						  _armorDamageReduction(5){
	std::srand(time(0));
	std::ostringstream ostr;
	ostr << this->_serialNumber;
	this->_name += "#";
	this->_name += ostr.str();
	std::cout << this->_name << ": FragTrap Recompiling my combat code!" << std::endl;
}

FragTrap::FragTrap(std::string name): _hitPoints(100),
									  _maxHitPoints(100),
									  _maxEnergyPoints(100),
									  _energyPoints(100),
									  _level(1),
									  _meleeAttackDamage(30),
									  _rangedAttackDamage(20),
									  _armorDamageReduction(5){
	std::srand(time(0));
	this->_name = name;
	std::cout << this->_name << ": FragTrap Recompiling my combat code!" << std::endl;
}

FragTrap::FragTrap(FragTrap const &src)
{
	std::srand(time(0));
	*this = src;
	std::cout << "FragTrap: DUPLICATEEEEEED"<< std:: endl;
}

FragTrap		&FragTrap::operator=(FragTrap const	&rhs)
{
	if (this != &rhs)
	{
		this->_name = rhs._name;
		this->_hitPoints = rhs._hitPoints;
		this->_energyPoints = rhs._energyPoints;
		this->_level = rhs._level;
	}
	return (*this);
}

FragTrap::~FragTrap() {
	std::cout << this->_name << ": FragTrap I\'M DEAD I\'M DEAD OH MY GOD I\'M DEAD!" << std::endl;
}

void			FragTrap::takeDamage(unsigned int amount){
	signed long long int calcAmount = amount;
	calcAmount -= _armorDamageReduction;

	if (this->_hitPoints < 1) {
		std::cout << this->_name << ": I\'m too pretty to die!" << std::endl;
		return ;
	}

	if (calcAmount < 1)
		std::cout << this->_name << ": What\'s that smell? Oh wait, it's just you!" << std::endl;
	else {
		if (this->_hitPoints - calcAmount < 0) {
			std::cout << this->_name << ": I\'m too pretty to die!" << std::endl;
			_hitPoints = 0;
		}
		else {
			std::cout << this->_name << ": Extra ouch!" << std::endl;
			this->_hitPoints -= calcAmount;
		}
	}
}

void			FragTrap::beRepaired(unsigned int amount) {
	this->_hitPoints += amount;

	if (_hitPoints > _maxHitPoints) {
		std::cout << this->_name << ": Health over here!" << std::endl;
		_hitPoints = _maxEnergyPoints;
	}
	else
		std::cout << this->_name << ": Sweet life juice!" << std::endl;

	this->_energyPoints += amount;

	if (_energyPoints > _maxEnergyPoints) {
		std::cout << this->_name << ": Health over here!" << std::endl;
		_energyPoints = _maxEnergyPoints;
	}
	else
		std::cout << this->_name << ": Sweet life juice!" << std::endl;
}

unsigned int	FragTrap::meleeAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  << " at melee, causing " <<_meleeAttackDamage << " points of damage !" << std::endl;
	return (_meleeAttackDamage);
}

unsigned int	FragTrap::rangedAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  << " at range, causing " <<_rangedAttackDamage << " points of damage !" << std::endl;
	return (_rangedAttackDamage);
}

unsigned int	FragTrap::laserInfernoAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with laser Inferno, causing " << 40 << " points of damage !" << std::endl;
	return (40);
}

unsigned int	FragTrap::torgueFiestaAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with torgue Fiesta, causing " << 32 << " points of damage !" << std::endl;
	return (32);
}

unsigned int	FragTrap::pirateShipModeAttack(std::string const &target){
	int damage = _maxHitPoints + _energyPoints * (_level / 100);

	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with Pirate Ship Mode, causing " << damage << " points of damage !" << std::endl;
	return (damage);
}

unsigned int	FragTrap::vaulthunter_dot_exe(std::string const &target){

	if (_energyPoints < 25) {
		std::cout << "No energy to do this" << std::endl;
		return (0);
	}

	_energyPoints -=25;

	int rndNumber = std::rand() % 5;


	std::cout << rndNumber;
	if (rndNumber == 0)
		return meleeAttack(target);
	if (rndNumber == 1)
		return rangedAttack(target);
	if (rndNumber == 2)
		return laserInfernoAttack(target);
	if (rndNumber == 3)
		return torgueFiestaAttack(target);
//	if (rndNumber == 4)
	return pirateShipModeAttack(target);
}

//
//Let's get this party started!
//I'M DEAD I'M DEAD OHMYGOD I'M DEAD!
//O-KAY! Thanks for giving me a second chance, God. I really appreciate it.
//Are you god? Am I dead?