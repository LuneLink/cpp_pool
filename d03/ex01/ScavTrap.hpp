//
// Created by lunelink on 5/8/18.
//

#ifndef CPP_POOL_SCAVTRAP_HPP
#define CPP_POOL_SCAVTRAP_HPP

#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>

class ScavTrap {

private:
	static unsigned int	_serialNumber;
	static int			_challengeNumber;
	static std::string	_challenges[];

	unsigned int		_hitPoints;
	unsigned int 		_maxHitPoints;
	unsigned int 		_maxEnergyPoints;
	unsigned int		_energyPoints;
	unsigned int		_level;
	std::string			_name;
	unsigned int		_meleeAttackDamage;
	unsigned int		_rangedAttackDamage;
	unsigned int		_armorDamageReduction;

public:
	ScavTrap(void);
	ScavTrap(std::string name);
	ScavTrap(ScavTrap const & src);
	~ScavTrap();

	unsigned int		meleeAttack(std::string const &target);
	unsigned int		rangedAttack(std::string const &target);
	unsigned int		laserInfernoAttack(std::string const &target);
	unsigned int		torgueFiestaAttack(std::string const &target);
	unsigned int		pirateShipModeAttack(std::string const &target);

	void				takeDamage(unsigned int amount);
	void				beRepaired(unsigned int amount);
	void				challengeNewcomer(std::string const &target);

	ScavTrap &			operator=(ScavTrap const & rhs);

};


#endif
