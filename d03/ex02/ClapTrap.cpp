//
// Created by lunelink on 5/8/18.
//

#include "ClapTrap.hpp"

unsigned int ClapTrap::_serialNumber = 1;

ClapTrap::ClapTrap(void): _hitPoints(100),
						  _maxHitPoints(100),
						  _level(1),
						  _meleeAttackDamage(30),
						  _rangedAttackDamage(20),
						  _armorDamageReduction(5),
						  _energyPoints(100),
						  _maxEnergyPoints(100){
	std::srand(time(0));
	std::ostringstream ostr;
	ostr << this->_serialNumber;
	this->_name += "#";
	this->_name += ostr.str();
	std::cout << this->_name << ":CLAPTRAP Recompiling my combat code!" << std::endl;
}

ClapTrap::ClapTrap(std::string name, int hp, int en, int mad, int rad, int adr)
{
	std::srand(time(0));
	std::cout << "Creating ClapTrap" << std::endl;
	this->_hitPoints = hp;
	this->_maxHitPoints = hp;
	this->_energyPoints = en;
	this->_maxEnergyPoints = en;
	this->_level = 1;
	this->_meleeAttackDamage = mad;
	this->_rangedAttackDamage = rad;
	this->_armorDamageReduction = adr;
	this->_name = name;
}

ClapTrap::ClapTrap(std::string name): _hitPoints(100),
									  _maxHitPoints(100),
									  _level(1),
									  _meleeAttackDamage(30),
									  _rangedAttackDamage(20),
									  _armorDamageReduction(5),
									  _energyPoints(100),
									  _maxEnergyPoints(100){
	std::srand(time(0));
	this->_name = name;
	std::cout << this->_name << ":CLAPTRAP Recompiling my combat code!" << std::endl;
}

ClapTrap::~ClapTrap() {
	std::cout << this->_name << ": I\'M JUST A CLAPTRAP I\'M DEAD OHMYGOD I\'M DEAD!" << std::endl;
}

ClapTrap::ClapTrap(ClapTrap const &src)
{
	*this = src;
}

ClapTrap		&ClapTrap::operator=(ClapTrap const	&rhs)
{
	if (this != &rhs)
	{
		this->_maxHitPoints = rhs._maxHitPoints;
		this->_maxEnergyPoints = rhs._maxEnergyPoints;
		this->_armorDamageReduction = rhs._armorDamageReduction;
		this->_name = rhs._name;
		this->_hitPoints = rhs._hitPoints;
		this->_energyPoints = rhs._energyPoints;
		this->_level = rhs._level;
		this->_meleeAttackDamage = rhs._meleeAttackDamage;
		this->_rangedAttackDamage = rhs._rangedAttackDamage;
	}
	return (*this);
}





void			ClapTrap::takeDamage(unsigned int amount){
	signed long long int calcAmount = amount;
	calcAmount -= _armorDamageReduction;

	if (this->_hitPoints < 1) {
		std::cout << this->_name << ": I\'m too pretty to die!" << std::endl;
		return ;
	}

	if (calcAmount < 1)
		std::cout << this->_name << ": What\'s that smell? Oh wait, it's just you!" << std::endl;
	else {
		if (this->_hitPoints - calcAmount < 0) {
			std::cout << this->_name << ": I\'m too pretty to die!" << std::endl;
			_hitPoints = 0;
		}
		else {
			std::cout << this->_name << ": Extra ouch!" << std::endl;
			this->_hitPoints -= calcAmount;
		}
	}
}

void			ClapTrap::beRepaired(unsigned int amount) {
	this->_hitPoints += amount;

	if (_hitPoints > _maxHitPoints) {
		std::cout << this->_name << ": Health over here!" << std::endl;
		_hitPoints = _maxEnergyPoints;
	}
	else
		std::cout << this->_name << ": Sweet life juice!" << std::endl;

	this->_energyPoints += amount;

	if (_energyPoints > _maxEnergyPoints) {
		std::cout << this->_name << ": Health over here!" << std::endl;
		_energyPoints = _maxEnergyPoints;
	}
	else
		std::cout << this->_name << ": Sweet life juice!" << std::endl;
}

unsigned int	ClapTrap::meleeAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  << " at melee, causing " <<_meleeAttackDamage << " points of damage !" << std::endl;
	return (_meleeAttackDamage);
}

unsigned int	ClapTrap::rangedAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  << " at range, causing " <<_rangedAttackDamage << " points of damage !" << std::endl;
	return (_rangedAttackDamage);
}

unsigned int	ClapTrap::laserInfernoAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with laser Inferno, causing " << 40 << " points of damage !" << std::endl;
	return (40);
}

unsigned int	ClapTrap::torgueFiestaAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with torgue Fiesta, causing " << 32 << " points of damage !" << std::endl;
	return (32);
}

unsigned int	ClapTrap::pirateShipModeAttack(std::string const &target){
	int damage = _maxHitPoints + _energyPoints * (_level / 100);

	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with Pirate Ship Mode, causing " << damage << " points of damage !" << std::endl;
	return (damage);
}

//
//Let's get this party started!
//I'M DEAD I'M DEAD OHMYGOD I'M DEAD!
//O-KAY! Thanks for giving me a second chance, God. I really appreciate it.
//Are you god? Am I dead?