//
// Created by lunelink on 5/8/18.
//

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int main (void) {

	ClapTrap cTrap("dede");
	cTrap.takeDamage(10);

	ScavTrap sTrap("dede");
	sTrap.takeDamage(10);
	sTrap.challengeNewcomer("2313");

	FragTrap fTrap("dede");

	fTrap.vaulthunter_dot_exe("dede");
	fTrap.vaulthunter_dot_exe("dede");
	fTrap.vaulthunter_dot_exe("dede");
	fTrap.vaulthunter_dot_exe("dede");
	fTrap.vaulthunter_dot_exe("dede");
	fTrap.vaulthunter_dot_exe("dede");
	fTrap.vaulthunter_dot_exe("dede");

}
