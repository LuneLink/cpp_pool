#include "SuperTrap.hpp"

SuperTrap::SuperTrap(void): FragTrap(), NinjaTrap()
{
	_hitPoints = 100;
	_maxHitPoints = 100;
	_energyPoints = 120;
	_maxEnergyPoints = 120;
	_level = 1;
	_meleeAttackDamage = 60;
	_rangedAttackDamage = 20;
	_armorDamageReduction = 5;
	srand(time(NULL));
	std::cout << "SuperTrap Recompiling my combat code!" << std:: endl;
}

SuperTrap::SuperTrap(std::string name): FragTrap(name), NinjaTrap(name)
{
	_hitPoints = 100;
	_maxHitPoints = 100;
	_energyPoints = 120;
	_maxEnergyPoints = 120;
	_level = 1;
	_meleeAttackDamage = 60;
	_rangedAttackDamage = 20;
	_armorDamageReduction = 5;
	srand(time(NULL));
	std::cout << "SuperTrap Recompiling my combat code!" << std:: endl;
}

SuperTrap::~SuperTrap(){
	std::cout << this->_name << ": I\'M A SuperTrap I\'M DEAD OH MY GOD I\'M DEAD!" << std::endl;
}

SuperTrap::SuperTrap(SuperTrap const &src):
		ClapTrap(src._name, 100, 120, 60, 20, 5)
{
	std::cout << this->_name << ": SuperTrap Hey everybody! Check out my package! DUPLICATEEEEED" << std::endl;
}

SuperTrap		&SuperTrap::operator=(SuperTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}

void	SuperTrap::rangedAttack(std::string const & target) {

	FragTrap::rangedAttack(target);
}

void	SuperTrap::meleeAttack(std::string const & target) {

	NinjaTrap::rangedAttack(target);
}