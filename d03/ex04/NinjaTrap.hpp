//
// Created by lunelink on 5/13/18.
//

#ifndef CPP_POOL_NINJATRAP_HPP
#define CPP_POOL_NINJATRAP_HPP


#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : virtual public ClapTrap{
	public:
		NinjaTrap(void);
		NinjaTrap(std::string name);
		~NinjaTrap();
		NinjaTrap(NinjaTrap const &src);

		NinjaTrap		&operator=(NinjaTrap const &rhs);

		void			ninjaShoebox(ClapTrap &target) const;
		void			ninjaShoebox(FragTrap &target) const;
		void			ninjaShoebox(ScavTrap &target) const;
		void			ninjaShoebox(NinjaTrap &target) const;
};


#endif //CPP_POOL_NINJATRAP_HPP
