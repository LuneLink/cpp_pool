//
// Created by lunelink on 5/8/18.
//

#ifndef CPP_POOL_FRAGTRAP_HPP
#define CPP_POOL_FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap: virtual public ClapTrap {
	public:
							FragTrap(void);
							FragTrap(std::string name);
							~FragTrap();
							FragTrap(FragTrap const &src);

		FragTrap			&operator=(FragTrap const &rhs);

		unsigned int		vaulthunter_dot_exe(std::string const & target);
};


#endif
