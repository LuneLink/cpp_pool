//
// Created by lunelink on 5/8/18.
//

#include "SuperTrap.hpp"

int main (void) {

	SuperTrap superTrap("super");

	superTrap.beRepaired(10);

	superTrap.meleeAttack("wdwd");
	superTrap.rangedAttack("fef1213");

	return (1);
}
