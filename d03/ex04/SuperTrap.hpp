//
// Created by lunelink on 5/8/18.
//

#ifndef CPP_POOL_SUPERTRAP_HPP
#define CPP_POOL_SUPERTRAP_HPP

#include "FragTrap.hpp"
#include "NinjaTrap.hpp"
#include "ClapTrap.hpp"

class SuperTrap: virtual public FragTrap, virtual public NinjaTrap{

private:

public:
	SuperTrap(void);
	SuperTrap(std::string name);
	~SuperTrap();
	SuperTrap(SuperTrap const &src);

	SuperTrap		&operator=(SuperTrap const &rhs);

	void	rangedAttack(std::string const & target);
	void	meleeAttack(std::string const & target);
};


#endif
