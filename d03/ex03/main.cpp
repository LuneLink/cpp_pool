//
// Created by lunelink on 5/8/18.
//

#include "NinjaTrap.hpp"

int main (void) {

	NinjaTrap ninjaTrap("ninja");
	NinjaTrap ninjaTrap2("ninja2");
	ScavTrap scavTrap("scav");
	ClapTrap clapTrap("clap");
	FragTrap fragTrap("frag");

	ninjaTrap.ninjaShoebox(clapTrap);
	ninjaTrap.ninjaShoebox(fragTrap);
	ninjaTrap.ninjaShoebox(scavTrap);
	ninjaTrap.ninjaShoebox(ninjaTrap2);
	return (1);
}
