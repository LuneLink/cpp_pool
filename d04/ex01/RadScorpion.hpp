#ifndef RADSCORPION_H
# define RADSCORPION_H

# include <string>
# include <iostream>
# include "Enemy.hpp"

class RadScorpion: public Enemy
{
public:
    RadScorpion();
    ~RadScorpion();
    RadScorpion(RadScorpion const &src);

    RadScorpion				&operator=(RadScorpion const &rhs);
};

#endif