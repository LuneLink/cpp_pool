#ifndef SUPERMUTANT_H
# define SUPERMUTANT_H

# include <string>
# include <iostream>
# include "Enemy.hpp"

class SuperMutant: public Enemy
{
	public:
		SuperMutant();
		~SuperMutant();
		SuperMutant(SuperMutant const &src);

		SuperMutant				&operator=(SuperMutant const &rhs);

		virtual void		takeDamage(int damage);
};

#endif