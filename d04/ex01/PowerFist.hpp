//
// Created by LuneLink on 10/3/2018.
//

#ifndef CPP_POOL_POWERFIST_HPP
#define CPP_POOL_POWERFIST_HPP

#include "AWeapon.hpp"
#include <string>
#include <iostream>

class PowerFist: public AWeapon
{
public:
    PowerFist(void);
    ~PowerFist(void);
    PowerFist(PowerFist const &src);

    PowerFist				&operator=(PowerFist const &rhs);

    void					attack() const;
};

#endif
