//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef BOCALSTEROID_H
# define BOCALSTEROID_H

#include <iostream>
#include <string>
# include "IAsteroid.hpp"

class BocalSteroid: public IAsteroid
{
	public:
		BocalSteroid();
		~BocalSteroid();
		BocalSteroid(BocalSteroid const &src);

		BocalSteroid			&operator=(BocalSteroid const &rhs);

		std::string				beMined(DeepCoreMiner *laser) const;
		std::string				beMined(StripMiner *laser) const;

		std::string	const		getName() const;
};

#endif
