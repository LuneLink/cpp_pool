//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef MININGBARGE_H
# define MININGBARGE_H

# include "IMiningLaser.hpp"
# include "IAsteroid.hpp"

class MiningBarge
{
	private:
		int						_laser_count;
		IMiningLaser			*_lasers[4];

	public:
		MiningBarge();
		~MiningBarge();
		MiningBarge(MiningBarge const &src);

		MiningBarge				&operator=(MiningBarge const &rhs);

		void					equip(IMiningLaser *laser);
		void					mine(IAsteroid *asteroid) const;
};

#endif
