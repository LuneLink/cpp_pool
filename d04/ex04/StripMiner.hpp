//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef STRIPEMINER_HPP
#define STRIPEMINER_HPP

# include <string>
# include <iostream>
# include "IMiningLaser.hpp"

class StripMiner: public IMiningLaser
{
public:
	StripMiner();
	~StripMiner();
	StripMiner(StripMiner const &src);

	StripMiner				&operator=(StripMiner const &rhs);

	void					mine(IAsteroid *asteroid);
};

#endif
