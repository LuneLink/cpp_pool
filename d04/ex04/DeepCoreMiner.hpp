//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef DEEPCOREMINER_H
# define DEEPCOREMINER_H

# include <string>
# include <iostream>
# include "IMiningLaser.hpp"

class DeepCoreMiner: public IMiningLaser
{
public:
	DeepCoreMiner();
	~DeepCoreMiner();
	DeepCoreMiner(DeepCoreMiner const &src);

	DeepCoreMiner			&operator=(DeepCoreMiner const &rhs);

	void					mine(IAsteroid *asteroid);
};

#endif
