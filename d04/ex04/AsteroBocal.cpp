//
// Created by Serhii Petrenko on 10/5/18.
//

#include "AsteroBocal.hpp"

AsteroBocal::AsteroBocal()
{}

AsteroBocal::AsteroBocal(AsteroBocal const &src)
{
	*this = src;
}

AsteroBocal::~AsteroBocal()
{

}

AsteroBocal			&AsteroBocal::operator=(AsteroBocal const &rhs)
{
	(void)rhs;
	return (*this);
}


std::string				AsteroBocal::beMined(DeepCoreMiner *laser) const
{
	(void)laser;
	return ("Thorite");
}

std::string				AsteroBocal::beMined(StripMiner *laser) const
{
	(void)laser;
	return ("Flavium");
}

std::string	const		AsteroBocal::getName() const
{
	return ("AsteroBocal");
}