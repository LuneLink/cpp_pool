//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef IMININGLASER_HPP
# define IMININGLASER_HPP

#include "IAsteroid.hpp"

class IMiningLaser
{
	public:
		virtual ~IMiningLaser() {}
		virtual void mine(IAsteroid*) = 0;
};


#endif
