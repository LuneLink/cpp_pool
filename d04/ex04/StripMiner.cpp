//
// Created by Serhii Petrenko on 10/5/18.
//

#include "StripMiner.hpp"

StripMiner::StripMiner()
{

}

StripMiner::StripMiner(StripMiner const &src)
{
	*this = src;
}

StripMiner::~StripMiner()
{

}

StripMiner				&StripMiner::operator=(StripMiner const &rhs)
{
	(void)rhs;
	return (*this);
}

void					StripMiner::mine(IAsteroid *asteroid)
{
	std::cout << "* strip mining ... got " << asteroid->beMined(this) << " ! *" << std::endl;
}