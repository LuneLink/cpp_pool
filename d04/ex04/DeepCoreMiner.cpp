//
// Created by Serhii Petrenko on 10/5/18.
//

#include "DeepCoreMiner.hpp"


DeepCoreMiner::DeepCoreMiner()
{

}

DeepCoreMiner::DeepCoreMiner(DeepCoreMiner const &src)
{
	*this = src;
}

DeepCoreMiner::~DeepCoreMiner()
{

}

DeepCoreMiner			&DeepCoreMiner::operator=(DeepCoreMiner const &rhs)
{
	(void)rhs;
	return (*this);
}

void					DeepCoreMiner::mine(IAsteroid *asteroid)
{
	std::cout << "* mining deep ... got " << asteroid->beMined(this) << " ! *" << std::endl;
}