#ifndef ASTEROBOCAL_H
# define ASTEROBOCAL_H

# include <string>
# include "IAsteroid.hpp"

class AsteroBocal: public IAsteroid
{
public:
	AsteroBocal();
	~AsteroBocal();
	AsteroBocal(AsteroBocal const &src);

	AsteroBocal				&operator=(AsteroBocal const &rhs);

	std::string				beMined(DeepCoreMiner *laser) const;
	std::string				beMined(StripMiner *laser) const;

	std::string	const		getName(void) const;
};

#endif
