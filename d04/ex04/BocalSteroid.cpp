//
// Created by Serhii Petrenko on 10/5/18.
//

#include "BocalSteroid.hpp"

BocalSteroid::BocalSteroid()
{

}

BocalSteroid::BocalSteroid(BocalSteroid const &src)
{
	*this = src;
}

BocalSteroid::~BocalSteroid()
{}

BocalSteroid			&BocalSteroid::operator=(BocalSteroid const &rhs)
{
	(void)rhs;
	return (*this);
}


std::string				BocalSteroid::beMined(DeepCoreMiner *laser) const
{
	(void)laser;
	return ("Zazium");
}

std::string				BocalSteroid::beMined(StripMiner *laser) const
{
	(void)laser;
	return ("Krpite");
}

std::string	const		BocalSteroid::getName() const
{
	return ("BocalSteroid");
}