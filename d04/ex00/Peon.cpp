//
// Created by LuneLink on 10/3/2018.
//

#include "Peon.hpp"


Peon::Peon(std::string name):
        Victim(name)
{
    std::cout << "Zog zog." << std::endl;
}

Peon::Peon(Peon const &src):
        Victim(src)
{
    *this = src;
    std::cout << "Zog zog." << std::endl;
}

Peon::~Peon()
{
    std::cout << "Bleuark..." << std::endl;
}

Peon				&Peon::operator=(Peon const	&rhs)
{
    if (this != &rhs)
        Victim::operator=(rhs);
    return (*this);
}

void				Peon::getPolymorphed() const
{
    std::cout << this->_name
              << " has been turned into a pink pony !" << std::endl;
}