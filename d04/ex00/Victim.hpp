#ifndef VICTIM_H
# define VICTIM_H

# include <string>
# include <iostream>

class Victim
{
protected:
    std::string		_name;

public:
    Victim(std::string name);
    ~Victim();
    Victim(Victim const &src);

    Victim				&operator=(Victim const &rhs);

    virtual void		getPolymorphed() const;

    std::string	const	&getName() const;
};

std::ostream			&operator<<(std::ostream &os, Victim const &rhs);

#endif