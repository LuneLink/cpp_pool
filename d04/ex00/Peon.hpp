#ifndef PEON_H
# define PEON_H

# include <string>
# include <iostream>
# include "Victim.hpp"

class Peon: public Victim
{
public:
    Peon(std::string name);
    ~Peon();
    Peon(Peon const &src);

    Peon				&operator=(Peon const &rhs);

    void				getPolymorphed() const;
};

#endif