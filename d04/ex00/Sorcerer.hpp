#ifndef SORCERER_H
# define SORCERER_H

# include <string>
# include <iostream>
# include "Victim.hpp"

class Sorcerer
{
protected:
    std::string		_name;
    std::string		_type;

public:
    Sorcerer(std::string name, std::string type);
    ~Sorcerer();
    Sorcerer(Sorcerer const &src);

    Sorcerer			&operator=(Sorcerer const &rhs);

    void				polymorph(Victim const &victim) const;

    std::string	const	&getName() const;
    std::string	const	&getType() const;
};

std::ostream			&operator<<(std::ostream &os, Sorcerer const &rhs);

#endif