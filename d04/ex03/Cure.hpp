//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef CURE_H
# define CURE_H

# include <string>
# include <iostream>
# include "ICharacter.hpp"

class Cure: public AMateria
{
public:
	Cure();
	~Cure();
	Cure(Cure const &src);

	Cure				&operator=(Cure const &rhs);

	AMateria			*clone() const;
	void				use(ICharacter &target);
};

#endif
