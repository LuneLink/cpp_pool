//
// Created by Serhii Petrenko on 10/5/18.
//

#ifndef ICE_HPP
# define ICE_HPP

#include "AMateria.hpp"
#include <iostream>

class Ice: public AMateria
{
public:
	Ice();
	~Ice();
	Ice(Ice const &src);

	Ice					&operator=(Ice const &rhs);

	AMateria			*clone() const;
	void				use(ICharacter &target);
};

#endif //CPP_POOL_ICE_HPP
