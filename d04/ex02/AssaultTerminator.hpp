//
// Created by LuneLink on 10/4/2018.
//

#ifndef ASSAULTTERMINATOR_HPP
# define ASSAULTTERMINATOR_HPP
# include "ISpaceMarine.hpp"

class AssaultTerminator: public ISpaceMarine {
    public:
        AssaultTerminator();
        ~AssaultTerminator();
        AssaultTerminator(AssaultTerminator const &src);

        AssaultTerminator	&operator=(AssaultTerminator const &rhs);

        ISpaceMarine		*clone() const;
        void				battleCry() const;
        void				rangedAttack() const;
        void				meleeAttack() const;
    };


#endif
