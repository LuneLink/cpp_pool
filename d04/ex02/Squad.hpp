//
// Created by Serhii Petrenko on 10/4/18.
//

#ifndef CPP_POOL_SQUAD_HPP
#define CPP_POOL_SQUAD_HPP

#include "ISquad.hpp"
#

class Squad: public ISquad{

	private:

		struct _marine {
			ISpaceMarine*   unit;
			struct _marine* next;
		};
		struct _marine*     _list;
		struct _marine*		_last;
		int                 _listSize;

	public:

							Squad();
							Squad(Squad const& rhs);
							Squad& operator=(Squad const& rhs);
							~Squad();

		int           		getCount() const;
		int           		push(ISpaceMarine* unit);
		bool          		checkDup(ISpaceMarine* unit);
		ISpaceMarine* 		getUnit(int n) const;
};


#endif //CPP_POOL_SQUAD_HPP
