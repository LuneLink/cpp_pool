//
// Created by Serhii Petrenko on 10/4/18.
//

#ifndef TACTICALMARINE_HPP
#define TACTICALMARINE_HPP


#include "ISpaceMarine.hpp"

class TacticalMarine: public ISpaceMarine
{
	public:
		TacticalMarine();
		~TacticalMarine();
		TacticalMarine(TacticalMarine const &src);

		TacticalMarine	&operator=(TacticalMarine const &rhs);

		ISpaceMarine	*clone() const;
		void			battleCry() const;
		void			rangedAttack() const;
		void			meleeAttack() const;
};


#endif //CPP_POOL_TACTICALMARINE_HPP
