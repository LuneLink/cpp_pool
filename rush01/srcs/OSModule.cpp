//
// Created by Serhii Petrenko on 10/14/18.
//

#include "OSModule.hpp"

OSModule::OSModule() {
	initArr();
}

OSModule::~OSModule() {

}

OSModule::OSModule(OSModule const &src) {
	*this = src;
}

OSModule& OSModule::operator=(OSModule const &rhs) {
	this->_lastRecalculationTime = rhs._lastRecalculationTime;
	return (*this);
}

std::vector<Describe> &OSModule::getInfo() {
	return _osInfo;
}

void OSModule::initArr() {
	struct utsname os;

	uname(&os);
	std::string s(os.sysname);
	Describe sName;
	sName.name = "System Name";
	sName.info = s;

	Describe sRel;
	sRel.name = "Release(Kernel) Version";
	s = os.release;
	sRel.info = s;


	Describe osName;
	osName.name = "Release(Kernel) Version";
	std::system("defaults read /System/Library/CoreServices/SystemVersion ProductName > test.txt");
	std::ifstream my("test.txt");
	if (my)
	{
		std::getline(my, s);
		my.close();
	}
	else
		s = "Can't resolve OS name";
	osName.info =  s;

	Describe osVersion;
	osVersion.name = "OS Version";
	std::system("defaults read /System/Library/CoreServices/SystemVersion ProductVersion > test.txt");
	std::getline(my, s);
	std::ifstream my2("test.txt");
	if (my2)
	{
		std::getline(my2, s);
		my2.close();
	}
	else
		s = "Can't resolve OS Version";
	osVersion.info = s;
	std::system("rm -f test.txt");
	_osInfo.push_back(sName);
	_osInfo.push_back(sRel);
	_osInfo.push_back(osName);
	_osInfo.push_back(osVersion);
}