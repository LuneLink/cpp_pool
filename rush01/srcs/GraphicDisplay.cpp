#include "GraphicDisplay.hpp"

GraphicDisplay::GraphicDisplay(): index(0) {
	window.create(sf::VideoMode(800, 600, 32), "ft_gkrellm");
	controllers.push_back(new CPUSFMLController());
	controllers.push_back(new RAMSFMLController());
	controllers.push_back(new HOSTNAMESFMLController());
	controllers.push_back(new OSSFMLController());
	printf("fefffe");
}


GraphicDisplay::~GraphicDisplay(){
}

void GraphicDisplay::render() {
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
				index++;
				if (static_cast<int>(controllers.size()) - 1 < index)
					index = 0;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
				index--;
				if (index < 0)
					index = static_cast<int>(controllers.size()) - 1;
			}
		}

		if (index > -1) {
			window.clear();
			std::vector<sf::Text> vec = controllers[index]->getInfo();
			for (unsigned long i = 0; i < vec.size(); i++) {
				sf::Text txt = vec[i];
				const sf::String  str = txt.getString();
				window.draw(vec[i]);
			}
			window.display();
		}
	}

}