//
// Created by Serhii Petrenko on 10/14/18.
//

#include "OSSFMLController.hpp"

OSSFMLController::OSSFMLController() {

	if (!font.loadFromFile("./static/CaviarDreams.ttf"))
	{
		// error...
	}
	visual.push_back(sf::Text("Model: 0", font, 30));
	visual.push_back(sf::Text("Clock Speed: 0", font, 30));
	visual.push_back(sf::Text("Core Count: 0", font, 30));
	visual.push_back(sf::Text("Activity: 0", font, 30));

	visual[0].setPosition(2, 2);
	visual[1].setPosition(2, 40);
	visual[2].setPosition(2, 80);
	visual[3].setPosition(2, 120);
}

OSSFMLController::~OSSFMLController(){

}


std::vector<sf::Text> &OSSFMLController::getInfo() {
	std::vector<Describe> vec = info.getInfo();
	for (unsigned long i = 0; i < vec.size(); i++) {
		std::string str(vec[i].name + ": " + vec[i].info);
		sf::String str2(str.c_str());
		visual[i].setString(str2);
	}
	return visual;
}