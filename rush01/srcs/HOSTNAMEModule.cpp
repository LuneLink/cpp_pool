//
// Created by Serhii Petrenko on 10/14/18.
//

#include "HOSTNAMEModule.hpp"

HOSTNAMEModule::HOSTNAMEModule(){


	Describe model = initHostName();
	Describe clockSpeed = initLogin();

	_hostInfo.push_back(model);
	_hostInfo.push_back(clockSpeed);
	_hostInfo.push_back(clockSpeed);
}

HOSTNAMEModule::~HOSTNAMEModule() {

}

HOSTNAMEModule::HOSTNAMEModule(HOSTNAMEModule const &src) {
	*this = src;
}

HOSTNAMEModule& HOSTNAMEModule::operator=(HOSTNAMEModule const &rhs) {
	this->_hostInfo = rhs._hostInfo;
	return (*this);
}

std::vector<Describe> &HOSTNAMEModule::getInfo() {
	Describe		currentTime;
	std::time_t		now = time(0);
	char			buf[80];
	std::string		t;

	currentTime.name = "Date";

	std::strftime(buf, sizeof(buf), "%Y-%m-%d %X", std::localtime(&now));
	t = buf;
	currentTime.info = t;
	_hostInfo[2] = currentTime;


	return _hostInfo;
}

Describe	HOSTNAMEModule::initHostName(){
	Describe model;

	model.name = "Host Name";

	char hostname[_SC_HOST_NAME_MAX];
	if (gethostname(hostname, _SC_HOST_NAME_MAX))
	{

	}
	std::string str = hostname;
	model.info = str;
	return model;
}

Describe	HOSTNAMEModule::initLogin(){
	Describe model;

	model.name = "Login";

	char login[_SC_LOGIN_NAME_MAX];
	if (getlogin_r(login, _SC_LOGIN_NAME_MAX)) {

	}
	std::string str = login;
	model.info = str;
	return model;
}
