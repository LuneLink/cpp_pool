#include "NETModule.hpp"

NETModule::NETModule(){

}

NETModule::~NETModule() {

}

NETModule::NETModule(NETModule const &src) {
	*this = src;
}

NETModule& NETModule::operator=(NETModule const &rhs) {
	this->_netInfo = rhs._netInfo;
	return (*this);
}

std::vector<Describe> &NETModule::getInfo() {
	return this->_netInfo;
}
