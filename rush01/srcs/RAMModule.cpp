
#include "RAMModule.hpp"

RAMModule::RAMModule() {
	_lastRecalculationTime = time (NULL);

	Describe size = initTotalSize();
	_ramInformation.push_back(size);
	Describe freeSize = initFreeSize();
	_ramInformation.push_back(freeSize);
	Describe usageSize = initUsageSize();
	_ramInformation.push_back(usageSize);
	Describe activity = initActivity();
	_ramInformation.push_back(activity);
}

RAMModule::~RAMModule() {

}

RAMModule::RAMModule(RAMModule const &src) {
	*this = src;
}

RAMModule& RAMModule::operator=(RAMModule const &rhs) {
	this->_lastRecalculationTime = rhs._lastRecalculationTime;
	return (*this);
}

std::vector<Describe> &RAMModule::getInfo() {
	time_t currentTime = time(NULL);
	if (difftime(currentTime, _lastRecalculationTime) >= 9)
	{
		_ramInformation[1] = initFreeSize();
		_ramInformation[2] = initUsageSize();
		_ramInformation[3] = initActivity();
		_lastRecalculationTime = currentTime;
	}
	return _ramInformation;
}

//-------------------------PRIVATE------------------------------------------

Describe RAMModule::initTotalSize() {
	Describe			totalSize;
	size_t				size;
	size_t				bufferlen = sizeof(size);
	std::stringstream	ss;

	totalSize.name = "RAM Size";

	sysctlbyname("hw.memsize", &size, &bufferlen, NULL, 0);
	ss << size;
	totalSize.info = ss.str();
	return totalSize;
}

Describe RAMModule::initFreeSize() {
	vm_size_t				page_size;
	mach_port_t				mach_port;
	mach_msg_type_number_t	count;
	vm_statistics64_data_t	vm_stats;
	Describe				freeSize;
	std::stringstream		ss;

	freeSize.name = "FreeSize";
	mach_port = mach_host_self();
	count = sizeof(vm_stats) / sizeof(natural_t);
	if (KERN_SUCCESS == host_page_size(mach_port, &page_size) &&
		KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO64,
										  (host_info64_t)&vm_stats, &count)) {
		long long free = (vm_stats.free_count + vm_stats.inactive_count) * vm_page_size;
		ss << free;
		freeSize.info = ss.str();
	}
	else
		freeSize.info = "0";
	return freeSize;
}

Describe RAMModule::initUsageSize() {
	Describe			usageSize;
	std::stringstream	ss;

	usageSize.name = "Usage Size";
	long long total = atoll(_ramInformation[0].info.c_str());
	long long totalFree = atoll(_ramInformation[1].info.c_str());
	ss << total - totalFree;
	usageSize.info = ss.str();
	return usageSize;
}

Describe RAMModule::initActivity() {
	Describe			activity;
	std::stringstream	ss;

	activity.name = "Activity";
	long long total = atoll(_ramInformation[0].info.c_str());
	long long totalFree = atoll(_ramInformation[1].info.c_str());
	ss << 100 - ((totalFree * 100.) / total);
	activity.info = ss.str();
	return activity;
}

