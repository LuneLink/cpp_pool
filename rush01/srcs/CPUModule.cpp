#include "../includes/CPUModule.hpp"

#include <iostream>

CPUModule::CPUModule(): _prevTotalTick(0), _prevIdleTicks(0) {

	_lastRecalculationTime = time (NULL);

	Describe model = initModel();
	Describe clockSpeed = initClockSpeed();
	Describe coreCount = initCoreCount();
	Describe activity = initActivity();

//	_processorInformation.insert(std::pair<std::string, std::string>("Model", "model1"));
//	_processorInformation.insert(std::pair<std::string, std::string>("Clock Speed", "3212"));
//	_processorInformation.insert(std::pair<std::string, std::string>("Core Count", "1"));
//	_processorInformation.insert(std::pair<std::string, std::string>("Activity", "4.22"));
	_processorInformation.push_back(model);
	_processorInformation.push_back(clockSpeed);
	_processorInformation.push_back(coreCount);
	_processorInformation.push_back(activity);
}

CPUModule::~CPUModule() {

}

CPUModule::CPUModule(CPUModule const &src) {
	*this = src;
}

CPUModule& CPUModule::operator=(CPUModule const &rhs) {
	this->_prevTotalTick = rhs._prevTotalTick;
	this->_prevIdleTicks = rhs._prevIdleTicks;
	this->_processorInformation = rhs._processorInformation;
	return (*this);
}

std::vector<Describe> &CPUModule::getInfo() {
	time_t currentTime = time(NULL);
	if (difftime(currentTime, _lastRecalculationTime) >= 9)
	{
		_processorInformation[3] = initActivity();
		_lastRecalculationTime = currentTime;
	}
	return _processorInformation;
}

//-----------------------------------Private---------------------------------

Describe	CPUModule::initModel(){
	Describe model;

	model.name = "Model";

	char buffer[256];
	size_t bufferlen = 256;

	sysctlbyname("machdep.cpu.brand_string", &buffer, &bufferlen, NULL, 0);
	std::stringstream ss;
	ss << buffer;
	model.info = ss.str();
	return model;
}

Describe	CPUModule::initClockSpeed(){
	Describe			clockSpeed;
	unsigned long		freeq;
	size_t				bufferlen = sizeof(freeq);
	std::stringstream	ss;

	clockSpeed.name = "Clock Speed";
	sysctlbyname("hw.cpufrequency", &freeq, &bufferlen, NULL, 0);
	ss << static_cast<float>(freeq) / 1000000000;
	clockSpeed.info = ss.str();

	return clockSpeed;
}

Describe	CPUModule::initCoreCount(){

	Describe			coreCount;
	int					freeq;
	size_t				bufferlen = sizeof(freeq);
	std::stringstream	ss;

	coreCount.name = "Core Count";
	sysctlbyname("machdep.cpu.core_count", &freeq, &bufferlen, NULL, 0);
	ss << freeq;
	coreCount.info = ss.str();
	return coreCount;

}

Describe	CPUModule::initActivity() {
	Describe			coreCount;
	std::stringstream	ss;

	coreCount.name = "Activity";

//	struct rusage r_usage;
//
//	if (getrusage(RUSAGE_SELF, &r_usage)) {
//		/* ... error handling ... */
//	}
//
//	float a =  (r_usage.ru_stime.tv_sec + static_cast<float>(r_usage.ru_stime.tv_usec) / 10000 ) * 10;
//	std::cout << r_usage.ru_stime.tv_sec << std::endl;
//	std::cout << r_usage.ru_stime.tv_usec << std::endl;
//	ss << a;
//	coreCount.info = ss.str();
//	return coreCount;




	std::string activity;
	static double oTotal = 0;
	static double oWork = 0;
	natural_t cpuCount;
	processor_cpu_load_info_t cpuInfo;
	mach_msg_type_number_t nbInfo;
	kern_return_t ret = host_processor_info(mach_host_self(), PROCESSOR_CPU_LOAD_INFO, &cpuCount, reinterpret_cast<processor_info_array_t *>(&cpuInfo), &nbInfo);
	if (ret != KERN_SUCCESS) {
		activity = "0";
	}

	size_t system = 0;
	size_t user = 0;
	size_t idle = 0;
	size_t totalSystemTime = 0;
	size_t totalUserTime = 0;
	size_t totalIdleTime = 0;
	for (natural_t i = 0; i < cpuCount; i++) {
		system = cpuInfo[i].cpu_ticks[CPU_STATE_SYSTEM];
		user = cpuInfo[i].cpu_ticks[CPU_STATE_USER] + cpuInfo[i].cpu_ticks[CPU_STATE_NICE];
		idle = cpuInfo[i].cpu_ticks[CPU_STATE_IDLE];

		totalSystemTime += system;
		totalUserTime += user;
		totalIdleTime += idle;
	}

	double nTotal = totalIdleTime + totalSystemTime + totalUserTime;
	double nWork = totalSystemTime + totalUserTime;

	double _activity = (nWork - oWork)/(nTotal - oTotal) * 100;
	std::stringstream activeS;
	activeS << _activity;
	activity = activeS.str();

	oTotal = nTotal;
	oWork = nWork;

	ss << _activity;
	coreCount.info = ss.str();
	return (coreCount);
}