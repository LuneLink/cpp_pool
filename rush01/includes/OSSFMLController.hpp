//
// Created by Serhii Petrenko on 10/14/18.
//

#ifndef CPP_POOL_OSSFMLCONTROLLER_HPP
#define CPP_POOL_OSSFMLCONTROLLER_HPP

#include "ISFMLController.hpp"
#include "OSModule.hpp"


class OSSFMLController: public ISFMLController{

	public:
		OSSFMLController();
		~OSSFMLController();

		std::vector<sf::Text> &getInfo();

	private:
		std::vector <sf::Text>	visual;
		OSModule				info;
		sf::Font				font;

};


#endif

