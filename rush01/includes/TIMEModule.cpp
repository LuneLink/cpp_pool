#include "TIMEModule.hpp"

TIMEModule::TIMEModule() {
	Describe time = getCurrentTime();
	_timeInfo.push_back(time);
}

TIMEModule::~TIMEModule() {

}

TIMEModule::TIMEModule(TIMEModule const &src) {
	*this = src;
}

TIMEModule& TIMEModule::operator=(TIMEModule const &rhs) {
	this->_timeInfo = rhs._timeInfo;
	return (*this);
}

std::vector<Describe> &TIMEModule::getInfo()  {
	_timeInfo[0] = getCurrentTime();
	return _timeInfo;
}

//----------------------------------Private---------------------------------


Describe TIMEModule::getCurrentTime(){
	Describe		currentTime;
	std::time_t		now = time(0);
	char			buf[80];
	std::string		t;

	currentTime.name = "Date";

	std::strftime(buf, sizeof(buf), "%Y-%m-%d %X", std::localtime(&now));
	t = buf;
	currentTime.info = t;
	return currentTime;
}