#ifndef IMONITORMODULE_HPP
# define IMONITORMODULE_HPP

#include <string>
#include <vector>

struct Describe {
	std::string name;
	std::string info;
};

class IMonitorModule {
	public:
		virtual ~IMonitorModule() {}
		virtual std::vector<Describe> &getInfo() = 0;
};


#endif