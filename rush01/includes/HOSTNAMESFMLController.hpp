//
// Created by Serhii Petrenko on 10/14/18.
//

#ifndef CPP_POOL_HOSTNAMESFMLCONTROLLER_HPP
#define CPP_POOL_HOSTNAMESFMLCONTROLLER_HPP

#include "HOSTNAMEModule.hpp"
#include <SFML/Graphics.hpp>
#include "ISFMLController.hpp"

class HOSTNAMESFMLController :public ISFMLController{
	public:
		HOSTNAMESFMLController();
		~HOSTNAMESFMLController();

		std::vector<sf::Text> &getInfo();

	private:
		std::vector <sf::Text>	visual;
		HOSTNAMEModule			info;
		sf::Font				font;
};


#endif //CPP_POOL_HOSTNAMESFMLCONTROLLER_HPP
