#ifndef TIMEMODULE_HPP
# define TIMEMODULE_HPP

#include "IMonitorModule.hpp"
#include <ctime>

class TIMEModule : public IMonitorModule{

	public:
		TIMEModule ();
		TIMEModule(TIMEModule const &src);
		~TIMEModule();

		TIMEModule &operator=(TIMEModule const &rhs);

		std::vector<Describe> &getInfo();

	private:
		Describe getCurrentTime();

	private:
		std::vector<Describe> _timeInfo;
};


#endif //CPP_POOL_TIMEMODULE_HPP
