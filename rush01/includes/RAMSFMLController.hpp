//
// Created by Serhii Petrenko on 10/14/18.
//

#ifndef CPP_POOL_RAMSFMLCONTROLLER_HPP
#define CPP_POOL_RAMSFMLCONTROLLER_HPP

#include "ISFMLController.hpp"
#include "RAMModule.hpp"

# include <mach/host_info.h>
# include <mach/mach_error.h>
# include <mach/mach_host.h>
# include <mach/vm_map.h>


class RAMSFMLController: public ISFMLController{

public:
	RAMSFMLController();
	~RAMSFMLController();

	std::vector<sf::Text> &getInfo();

private:
	std::vector <sf::Text>	visual;
	RAMModule				info;
	sf::Font				font;
};


#endif
