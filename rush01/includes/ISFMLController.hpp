//
// Created by Serhii Petrenko on 10/14/18.
//

#ifndef ICONTROLLER_HPP
# define ICONTROLLER_HPP

#include <SFML/Graphics.hpp>

class ISFMLController {
	public:
		virtual ~ISFMLController() {}
		virtual std::vector<sf::Text> &getInfo() = 0;
};


#endif //CPP_POOL_ICONTROLLER_HPP
