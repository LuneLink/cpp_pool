#ifndef CPUMODULE_HPP
# define CPUMODULE_HPP

# include <sys/sysctl.h>
# include <sys/resource.h>
# include <sstream>
# include <time.h>
# include <mach/host_info.h>
# include <mach/mach_error.h>
# include <mach/mach_host.h>
# include <mach/vm_map.h>

# include "IMonitorModule.hpp"

class CPUModule : public IMonitorModule{

	public:
		CPUModule ();
		CPUModule(CPUModule const &src);
		~CPUModule();

		CPUModule &operator=(CPUModule const &rhs);

//		std::map<std::string, std::string> &getInfo();
		std::vector<Describe> &getInfo();

	private:
		Describe	initModel();
		Describe	initClockSpeed();
		Describe	initCoreCount();
		Describe	initActivity();


	private:
		std::vector<Describe>	_processorInformation;
		unsigned long long		_prevTotalTick;
		unsigned long long		_prevIdleTicks;
		time_t					_lastRecalculationTime;
};


#endif
