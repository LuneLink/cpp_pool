#ifndef RAMMODULE_HPP
# define RAMMODULE_HPP

# include <sys/sysctl.h>
# include <sstream>
# include <mach/host_info.h>
# include <mach/mach_error.h>
# include <mach/mach_host.h>
# include <mach/vm_map.h>

# include "RAMModule.hpp"
# include "IMonitorModule.hpp"

	class RAMModule : public IMonitorModule{

	public:
		RAMModule ();
		RAMModule(RAMModule const &src);
		~RAMModule();

		RAMModule &operator=(RAMModule const &rhs);

	//		std::map<std::string, std::string> &getInfo();
		std::vector<Describe> &getInfo();

	private:
		Describe	initTotalSize();
		Describe	initFreeSize();
		Describe	initUsageSize();
		Describe	initActivity();

	private:
		std::vector<Describe>	_ramInformation;
		time_t					_lastRecalculationTime;
	};


#endif
