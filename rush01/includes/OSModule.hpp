#ifndef CPP_POOL_OSMODULE_HPP
# define CPP_POOL_OSMODULE_HPP

# include <sys/sysctl.h>
# include <sstream>
# include <mach/host_info.h>
# include <mach/mach_error.h>
# include <mach/mach_host.h>
# include <mach/vm_map.h>

# include "RAMModule.hpp"
# include "IMonitorModule.hpp"
# include <sys/utsname.h>
# include <fstream>
# include <cstdlib>
# include <string>

class OSModule : public IMonitorModule{

public:
	OSModule ();
	OSModule(OSModule const &src);
	~OSModule();

	OSModule &operator=(OSModule const &rhs);

	std::vector<Describe> &getInfo();

private:
	void	initArr();

private:
	std::vector<Describe>	_osInfo;
	time_t					_lastRecalculationTime;
};

#endif