#ifndef SFMLCONTROLLER_HPP
# define SFMLCONTROLLER_HPP

#include "ISFMLController.hpp"
#include "CPUModule.hpp"
#include <vector>


class CPUSFMLController: public ISFMLController{

	public:
		CPUSFMLController();
		~CPUSFMLController();

		std::vector<sf::Text> &getInfo();

	private:
		std::vector <sf::Text>	visual;
		CPUModule				info;
		sf::Font				font;
};


#endif //CPP_POOL_SFMLCONTROLLER_HPP
