//
// Created by Serhii Petrenko on 10/14/18.
//

#ifndef GRAPHICDISPLAY_HPP
# define GRAPHICDISPLAY_HPP

# include "IMonitorDisplay.hpp"
# include "ISFMLController.hpp"
# include "CPUSFMLController.hpp"
# include "RAMSFMLController.hpp"
# include "HOSTNAMESFMLController.hpp"
# include "OSSFMLController.hpp"
# include <SFML/Graphics.hpp>

class GraphicDisplay : public IMonitorDisplay{

public:
	GraphicDisplay();
	~GraphicDisplay();
	void render();

	private:
		sf::RenderWindow				window;
		std::vector<ISFMLController*>	controllers;
		int								index;
};


#endif //CPP_POOL_GRAPHICDISPLAY_HPP
