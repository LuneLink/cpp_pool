#ifndef HOSTNAMEMODULE_HPP
# define HOSTNAMEMODULE_HPP

# include "IMonitorModule.hpp"
# include <unistd.h>
# include <climits>
# include <ctime>

class HOSTNAMEModule: public IMonitorModule {
	public:
		HOSTNAMEModule ();
		HOSTNAMEModule(HOSTNAMEModule const &src);
		~HOSTNAMEModule();

		HOSTNAMEModule &operator=(HOSTNAMEModule const &rhs);

	//		std::map<std::string, std::string> &getInfo();
		std::vector<Describe> &getInfo();

	private:
		Describe	initHostName();
		Describe	initLogin();


	private:
		std::vector<Describe>	_hostInfo;

};


#endif
