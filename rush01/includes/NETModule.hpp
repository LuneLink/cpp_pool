#ifndef NETMODULE_HPP
# define NETMODULE_HPP

# include "IMonitorModule.hpp"

class NETModule: public IMonitorModule {
	public:
		NETModule ();
		NETModule(NETModule const &src);
		~NETModule();

		NETModule &operator=(NETModule const &rhs);

		std::vector<Describe> &getInfo();

	private:
		Describe getCurrentTime();

	private:
		std::vector<Describe> _netInfo;
};


#endif //CPP_POOL_NETMODULE_HPP
