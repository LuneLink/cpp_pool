#ifndef CPP_POOL_EVALEXP_H
#define CPP_POOL_EVALEXP_H

#include <string>
#include <sstream>
#include <iostream>

#include "Stack.class.hpp"
#include "Fixed.class.hpp"


class EvalExp {

public:
			EvalExp(std::string exp);
	void	getResult();

private:
	bool						removeSpacesCheckIsValid(std::string &str);
	bool						isValidSymbol(char symbol);
	int							getOperationPriority(char operation);
	void						calculate(std::string exp);
	void						pushOperations(char symb);
	void						pushNumber(std::string &str);
	void						reverse(std::string &str);
	void						makeOperation();
	void						makeOperationToBrascket();

	FixedNumberStack			numberStack;
	SymbolStack					symbolStack;
	static const std::string	validSymbols;
	Fixed						result;

	bool						hasError;
};


#endif //CPP_POOL_EVALEXP_H
