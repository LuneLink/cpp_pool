#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <sstream>
#include <string>

#include "EvalExp.class.hpp"
#include "Stack.class.hpp"

int main(int argc, char **argv)
{
//	//std::string str = " 14.323 + 23 ";
//
//	char *str = "1+2*(3 + 4/2 - (1 + 2))*2 + 1";
//
//	//char *str = "(18.18 + 3.03) * 2";
//

	if (argc == 2)
	{
		EvalExp exp(argv[1]);
		exp.getResult();
	}
	return (0);
}