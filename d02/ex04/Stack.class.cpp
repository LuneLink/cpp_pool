
#include "Stack.class.hpp"



StackS::StackS(char symbol) {
	this->symbol = symbol;
	this->next = 0;
}

StackN::StackN(Fixed number) {
	this->number = number;
	this->next = 0;
}

StackN &StackN::operator=(StackN const & rhs)
{

	if (this != &rhs) {
		this->number = rhs.number;
		this->next = rhs.next;
	}

	return *this;
}

StackS &StackS::operator=(StackS const & rhs)
{

	if (this != &rhs) {
		this->symbol = rhs.symbol;
		this->next = rhs.next;
	}

	return *this;
}

SymbolStack::SymbolStack() {
	this->stack = 0;
}

void		SymbolStack::pushBack(char symbol){
	StackS *iter;

	iter = this->stack;
	if (!iter)
		this->stack = new StackS(symbol);
	else {
		while (iter->next)
			iter = iter->next;
		iter->next = new StackS(symbol);
	}
}

void		SymbolStack::print(){
	StackS *iter = this->stack;

	while(iter){
		std::cout << '{' << iter->symbol << '}';
		iter = iter->next;
	}
	std::cout << std::endl;
}

char		SymbolStack::lookLast(){
	StackS 	*iter;

	iter = this->stack;

	while (iter->next) {
		iter = iter->next;
	}

	return iter->symbol;
}

char		SymbolStack::pop(){
	StackS 	*iter;
	StackS 	*prev_iter = 0;

	iter = this->stack;
	prev_iter = iter;

	if (!iter->next)
	{
		char retSymb = iter->symbol;
		delete iter;
		this->stack = 0;
		return retSymb;
	}


	while (iter->next) {
		prev_iter = iter;
		iter = iter->next;
	}


	char retSymb = iter->symbol;
	delete iter;

	prev_iter->next = 0;

	return retSymb;
}

int			SymbolStack::size(){
	int size = 0;
	StackS *iter = this->stack;

	while (iter){
		iter = iter->next;
		size++;
	}
	return size;
}

bool		SymbolStack::isEmpty(){
	return this->stack == 0;
}

//--------------------------------FIXED------------------------------

int			FixedNumberStack::size(){
	int size = 0;
	StackN *iter = this->stack;

	while (iter){
		iter = iter->next;
		size++;
	}
	return size;
}

FixedNumberStack::FixedNumberStack(){
	this->stack = 0;
}

void		FixedNumberStack::print(){
	StackN *iter = this->stack;

	while(iter){
		std::cout << '{' << iter->number << '}';
		iter = iter->next;
	}
	std::cout << std::endl;
}


void		FixedNumberStack::pushBack(Fixed number){
	StackN *iter;

	iter = this->stack;
	if (!iter)
		this->stack = new StackN(number);
	else {
		while (iter->next)
			iter = iter->next;
		iter->next = new StackN(number);
	}
}

bool		FixedNumberStack::isEmpty(){
	return this->stack != 0;
}

Fixed		FixedNumberStack::pop(){
	StackN 	*iter = this->stack;
	StackN 	*prev_iter = iter;

	if (!iter->next)
	{
		Fixed retSymb = iter->number;
		delete iter;
		this->stack = 0;
		return retSymb;
	}

	while (iter->next) {
		prev_iter = iter;
		iter = iter->next;
	}

	Fixed retSymb = iter->number;
	delete iter;

	prev_iter->next = 0;

	return retSymb;
}

