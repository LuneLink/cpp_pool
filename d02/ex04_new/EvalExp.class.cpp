//
// Created by lunelink on 5/1/18.
//

#include "EvalExp.class.hpp"

const std::string EvalExp::validSymbols = "+-/*()0123456789.";

bool	isSpace(char symb) {
	return  (symb == ' ' || symb == '\t' ||
			symb == '\n' || symb =='\v' ||
			symb == '\f' || symb == '\r');

}

bool	isDigit(char symbol){
	return (symbol > 47 && symbol < 58);
}

bool	EvalExp::isValidSymbol(char symbol, char prevSymbol){
	static std::string signs = "+-*/";
	if (signs.find(prevSymbol) != std::string::npos && signs.find(symbol) != std::string::npos)
		return (false);
	return symbol == 0 || (validSymbols.find(symbol) != std::string::npos);
}

bool	EvalExp::validate(std::string &str){
	std::string		finalStr;
	unsigned long	length = str.length();
	char			prevSymbol = 0;
	int				brace = 0;

	for (unsigned i = 0; i < length; i++) {
		if (!isSpace(str[i])) {
			if (!isValidSymbol(str[i], prevSymbol))
				return false;
			finalStr.push_back(str[i]);
			prevSymbol = str[i];
			if (prevSymbol == '(')
				brace++;
			else if (prevSymbol == ')')
				brace--;
		}
	}
	str = finalStr;
	return brace == 0;
}


int		EvalExp::getOperationPriority(char operation){
	if (operation == '+' || operation == '-')
		return 1;
	if (operation == '*' || operation == '/')
		return 2;
	if (operation == '(')
		return 999;
	return (0);
}

void	EvalExp::pushOperations(char symb){

	if (symbolStack.isEmpty())
		symbolStack.pushBack(symb);
	else {

		char lastSymb = symbolStack.pop();

		if (getOperationPriority(lastSymb) < getOperationPriority(symb) || lastSymb == '(') {
			symbolStack.pushBack(lastSymb);
			symbolStack.pushBack(symb);
		}
		else {
			if (numberStack.size() < 2)
				hasError = true;
			else {symbolStack.pushBack(lastSymb);
				makeOperation();
				symbolStack.pushBack(symb);
			}
		}
	}
}

void	EvalExp::pushNumber(std::string &str){
	std::string numb;
	int i = str.length() - 1;
	bool wasADot = false;

	if (!isDigit(str[i])){
		hasError = true;
		return ;
	}
	for (;i >= 0; i--){
		if (str[i] == '.') {
			if (wasADot)
				hasError = true;
			wasADot = true;
		}
		else if (!isDigit(str[i]))
			break;
		numb.push_back(str[i]);
		str.erase(i, 1);
	}

	std::istringstream forConvert(numb);
	float convertedNumb;
	forConvert >> convertedNumb;
	numberStack.pushBack(convertedNumb);
	str.push_back('l');
}

void	EvalExp::makeOperation(){
	Fixed n2(numberStack.pop());
	Fixed n1(numberStack.pop());
	char operation = symbolStack.pop();

	if (operation == '+')
		numberStack.pushBack(n1 + n2);
	else if (operation == '-')
		numberStack.pushBack(n1 - n2);
	else if (operation == '*')
		numberStack.pushBack(n1 * n2);
	else if (operation == '/')
		numberStack.pushBack(n1 / n2);
}

void	EvalExp::reverse(std::string &str){
	unsigned length = str.length();

	for(unsigned i = 0; i < length / 2; i++){
		char symb = str[i];
		str[i] = str[length - i - 1];
		str[length - i - 1] = symb;
	}
}

void	EvalExp::makeOperationToBrascket() {
	while (!symbolStack.isEmpty()){
		if ((symbolStack.lookLast() == '(')) {
			symbolStack.pop();
			return;
		}
		makeOperation();
	}
	hasError = true;
}


void	EvalExp::calculate(std::string exp){
	int i = exp.length() - 1;

	for(; i >= 0; i--){
		if (hasError)
			return;
		if (exp[i] == ')')
			makeOperationToBrascket();
		else {
			int priority = getOperationPriority(exp[i]);

			if (priority != 0)
				pushOperations(exp[i]);
			else {
				if (exp[i] == '.') {
					hasError = true;
					return ;
				} else
					pushNumber(exp);
			}
		}
		i = exp.length() - 1;
		exp.erase(i, 1);
	}

	while(!symbolStack.isEmpty())
	{
		makeOperation();
//		Fixed popedNumb1 = numberStack.pop();
//		Fixed popedNumb2 = numberStack.pop();
//		numberStack.pushBack(makeOperation(popedNumb2, popedNumb1, symbolStack.pop()));
	}

	result = numberStack.pop();
}

void	EvalExp::getResult(){
	if (hasError)
		std::cout << "Error in expression!" << std::endl;
	else
		std::cout << result << std::endl;
}

EvalExp::EvalExp(std::string exp): numberStack(), symbolStack(), hasError(false){

		if (!exp.empty() && validate(exp)) {
			reverse(exp);
			calculate(exp);
		}
		else {
			this->hasError = true;
			std::cout << "Invalid string" << std::endl;
		}
}