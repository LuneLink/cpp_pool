//
// Created by lunelink on 5/6/18.
//

#ifndef CPP_POOL_STACK_H
#define CPP_POOL_STACK_H

#include "Fixed.class.hpp"

class StackS {
public:
	StackS	(char symbol);
	StackS	*next;
	char	symbol;
};

class StackN {
public:
	StackN	(Fixed number);
	StackN	*next;
	Fixed	number;
};

class			SymbolStack {

public:
				SymbolStack();
	void		pushBack(char symbol);
	char		pop();
	bool		isEmpty();
	char		lookLast();
	void		print();
	int			size();

private:
	StackS		*stack;
};

class			FixedNumberStack{

public:
				FixedNumberStack();
	void		pushBack(Fixed number);
	Fixed		pop();
	bool		isEmpty();
	void		print();
	int			size();

private:
	StackN	*stack;
};


#endif //CPP_POOL_STACK_H
