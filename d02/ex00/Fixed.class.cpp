#include "Fixed.class.hpp"

const int Fixed::_nBit = 8;

Fixed::Fixed(void)
{
    this->_rawBits = 0;
    std::cout << "Default Constructor called" << std::endl;
}

Fixed::Fixed(Fixed const &src){
    this->_rawBits = src.getRawBits();
    std::cout << "Copy constructor called" << std::endl;
}

Fixed::~Fixed() {
    std::cout << "Destructor called" << std::endl;
}

int			Fixed::getRawBits(void) const {
    std::cout << "getRawBits member function called" << std::endl;
    return this->_rawBits;
}

void		Fixed::setRawBits(int const raw) {
    this->_rawBits = raw;
}

Fixed	&Fixed::operator=(Fixed const &rhs) {
    std::cout << "Assignation operator called" << std::endl;
    if (this != &rhs)
        this->_rawBits = rhs.getRawBits();
    return *this;
}