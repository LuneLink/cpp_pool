#ifndef FIXED_CLASS_H
#define FIXED_CLASS_H

#include <iostream>
#include <string>

class Fixed {
    public:
        Fixed(void);
        Fixed(Fixed const & src);
        ~Fixed(void);

        Fixed &		operator=(Fixed const & rhs);
        int			getRawBits(void) const;
        void		setRawBits(int const raw);

    private:
        static int const	_nBit;
        int					_rawBits;
};

#endif
