#include "Fixed.class.hpp"
#include <iostream>
#include <cmath>

Fixed::Fixed(void): _rawBits(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(int const n)
{
	std::cout << "Int constructor called" << std::endl;
	setRawBits(n << _nBit);
}

Fixed::Fixed(float const n)
{
	std::cout << "Float constructor called" << std::endl;
	setRawBits(roundf(n * (1 << _nBit)));
}

Fixed::Fixed(Fixed const & src)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
}

Fixed::~Fixed(void)
{
	std::cout << "Destructor called" << std::endl;
}

int		Fixed::getRawBits(void) const
{
	return this->_rawBits;
}

void    Fixed::setRawBits(int const rawBits)
{
	this->_rawBits = rawBits;
}

int		Fixed::toInt(void) const
{
	return (getRawBits() >> this->_nBit);
}

float	Fixed::toFloat(void) const
{
	float	n;

	n = getRawBits();
	return (n / (1 << this->_nBit));
}

Fixed &		Fixed::operator=(Fixed const & rhs)
{
	std::cout << "Assignation operator called" << std::endl;
	if (this != &rhs)
		this->_rawBits = rhs.getRawBits();

	return *this;
}

bool	Fixed::operator>(Fixed const & rhs)
{
	return (this->_rawBits > rhs.getRawBits());
}

bool	Fixed::operator<(Fixed const & rhs)
{
	return (this->_rawBits < rhs.getRawBits());
}

bool	Fixed::operator>=(Fixed const & rhs)
{
	return (this->_rawBits >= rhs.getRawBits());
}

bool	Fixed::operator<=(Fixed const & rhs)
{
	return (this->_rawBits <= rhs.getRawBits());
}

bool	Fixed::operator==(Fixed const & rhs)
{
	return (this->_rawBits == rhs.getRawBits());
}

bool	Fixed::operator!=(Fixed const & rhs)
{
	return (this->_rawBits != rhs.getRawBits());
}

Fixed	Fixed::operator+(Fixed const & rhs)
{
	Fixed	ret;

	ret.setRawBits(this->_rawBits + rhs._rawBits);
	return (ret);
}

Fixed 	Fixed::operator-(Fixed const & rhs)
{
	Fixed   ret;

	ret.setRawBits(this->_rawBits - rhs._rawBits);
	return (ret);
}

Fixed 	Fixed::operator*(Fixed const & rhs)
{
	Fixed   ret;

	ret._raw = (this->_rawBits * rhs._rawBits) >> this->_nBit;
	return (ret);
}

Fixed 	Fixed::operator/(Fixed const & rhs)
{
	Fixed   ret;

	ret._rawBits = (this->_rawBits << this->_nBit) / rhs._rawBits;
	return (ret);
}

Fixed &		Fixed::operator++(void)
{
	this->_rawBits++;
	return (*this);
}

Fixed		Fixed::operator++(int)
{
	Fixed	newfix;

	newfix = *this;
	this->_rawBits++;
	return (newfix);
}

Fixed &		Fixed::operator--(void)
{
	this->_rawBits--;
	return (*this);
}

Fixed		Fixed::operator--(int)
{
	Fixed	newfix;

	newfix = *this;
	this->_rawBits++;
	return (newfix);
}

Fixed & 			Fixed::min(Fixed & lhs, Fixed & rhs)
{
	if (lhs.getRawBits() > rhs.getRawBits())
		return (rhs);
	return (lhs);
}

Fixed & 			Fixed::max(Fixed & lhs, Fixed & rhs)
{
	if (lhs.getRawBits() > rhs.getRawBits())
		return (lhs);
	return (rhs);
}

const Fixed & 	Fixed::min(Fixed const & lhs, Fixed const & rhs)
{
	if (lhs.getRawBits() > rhs.getRawBits())
		return (rhs);
	return (lhs);
}

const Fixed & 	Fixed::max(Fixed const & lhs, Fixed const & rhs)
{
	if (lhs.getRawBits() > rhs.getRawBits())
		return (lhs);
	return (rhs);
}

std::ostream &	operator<<(std::ostream & o, Fixed const & rhs)
{
	o << rhs.toFloat();
	return o;
}