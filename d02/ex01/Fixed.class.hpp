#ifndef FIXED_H
# define FIXED_H

#include <iostream>

class	Fixed {

public:

	Fixed(void);
	Fixed(int const n);
	Fixed(float const n);
	Fixed(Fixed const & src);
	~Fixed(void);

	Fixed &		operator=(Fixed const & rhs);
	bool		operator>(Fixed const & rhs);
	bool		operator<(Fixed const & rhs);
	bool		operator>=(Fixed const & rhs);
	bool		operator<=(Fixed const & rhs);
	bool		operator==(Fixed const & rhs);
	bool		operator!=(Fixed const & rhs);
	Fixed 		operator+(Fixed const & rhs);
	Fixed 		operator-(Fixed const & rhs);
	Fixed 		operator*(Fixed const & rhs);
	Fixed 		operator/(Fixed const & rhs);
	Fixed &		operator++(void);
	Fixed		operator++(int);
	Fixed &		operator--(void);
	Fixed		operator--(int);

	int		getRawBits(void) const;
	void	setRawBits(int const rawBits);
	float	toFloat(void) const;
	int		toInt(void) const;

private:

	static int const	_nBit = 8;
	int					_rawBits;

};

std::ostream &	operator<<(std::ostream & o, Fixed const & rhs);

#endif
