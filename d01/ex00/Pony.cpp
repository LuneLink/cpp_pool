//
// Created by lune on 8/25/17.
//

#include "Pony.hpp"

size_t Pony::_idCounter = 0;

Pony::Pony()
{
	this->_id = ++Pony::_idCounter;
	std::cout << "Call default constructor (Pony id = "<< _id << ")" << std::endl;
}

Pony::Pony(std::string name)
{
	this->_id = ++Pony::_idCounter;
	std::cout << "Call constructor with name " << name
			  << " (Pony id = "<< _id << ")" << std::endl;
}

Pony::~Pony()
{
	std::cout << "Call destructor (Pony id = " << _id <<")" << std::endl;
}

const std::string  &Pony::getName() const
{
	std::cout << "Call getName method (Pony id = " << _id <<")" << std::endl;
	return (_name);
}

void ponyOnTheHeap() {
	Pony *heapPony;

	heapPony = new Pony("Heap");
	heapPony->getName();
	delete heapPony;
}


void ponyOnThStack() {
	Pony stackPony("Stack");
	stackPony.getName();
}