//
// Created by lune on 8/25/17.
//

#ifndef CPP_POOL_PONY_HPP
#define CPP_POOL_PONY_HPP

#include <string>
#include <iostream>

class Pony
{
	public:
							Pony();
							Pony(std::string name);
							~Pony();
		const std::string   &getName() const;

	private:
		std::string			_name;
		size_t				_id;
		static size_t 		_idCounter;
};

void						ponyOnTheHeap();
void						ponyOnThStack();

#endif
