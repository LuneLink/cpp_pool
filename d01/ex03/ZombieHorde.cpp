//
// Created by lune on 8/25/17.
//

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n)
{
	srand(time(0));
	this->_zombieCount = n;
	this->_zombies = new Zombie[n];
	std::string name[3] = {"Archi", "Runio", "Branio"};

	for(int j = 0; j < n; j++)
		_zombies[j].setName(name[std::rand() % 3]);
}

ZombieHorde::~ZombieHorde()
{
	delete [] _zombies;
}

void	ZombieHorde::announce()
{
	for(int i = 0; i < _zombieCount; i++)
		_zombies[i].announce();
}