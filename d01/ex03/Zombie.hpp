#ifndef CPP_POOL_ZOMBIE_HPP
#define CPP_POOL_ZOMBIE_HPP

#include <string>
#include <iostream>

class Zombie
{
private:
	std::string 		_name;
	std::string			_type;

public:
						Zombie();
						Zombie (std::string _name, std::string _type);
	void				setName(std::string name);
	const std::string	&getName() const;
	void				setType(std::string name);
	const std::string	&getType() const;
	void 				announce();
};


#endif
