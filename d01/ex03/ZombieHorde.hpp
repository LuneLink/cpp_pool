//
// Created by lune on 8/25/17.
//

#ifndef CPP_POOL_ZOMBIEHORDE_HPP
#define CPP_POOL_ZOMBIEHORDE_HPP

#include "Zombie.hpp"
#include <time.h>
#include <cstdlib>

class ZombieHorde {

public:
			ZombieHorde(int n);
			~ZombieHorde();
	void	announce();

private:
	Zombie	*_zombies;
	int 	_zombieCount;
};


#endif
