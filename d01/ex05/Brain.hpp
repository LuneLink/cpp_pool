//
// Created by lune on 8/26/17.
//

#ifndef CPP_POOL_BRAIN_HPP
#define CPP_POOL_BRAIN_HPP

#include <string>
#include <sstream>

class Brain
{
private:
	std::string	_address;

public:
				Brain ();
	std::string	identify() const;
				~Brain();
};


#endif //CPP_POOL_BRAIN_HPP
