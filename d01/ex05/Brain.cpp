//
// Created by lune on 8/26/17.
//

#include "Brain.hpp"

Brain::Brain()
{
	std::stringstream	address2;
	size_t				len;

	address2  << (this);
	_address = address2.str();
	len = _address.length();
	for(size_t i = 2; i < len; i++)
		_address[i] = toupper(_address[i]);
}

Brain::~Brain()
{

}

std::string Brain::identify() const
{
	return _address;
}