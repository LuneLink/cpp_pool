//
// Created by lune on 8/26/17.
//

#ifndef CPP_POOL_HUMAN_HPP
#define CPP_POOL_HUMAN_HPP

#include "Brain.hpp"

class Human
{
private:
	const Brain brain;

public:
	std::string	identify() const;
	const Brain &getBrain() const;
};


#endif //CPP_POOL_HUMAN_HPP
