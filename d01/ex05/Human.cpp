//
// Created by lune on 8/26/17.
//

#include "Human.hpp"

std::string	Human::identify() const
{
	return (this->brain.identify());
}

const Brain &Human::getBrain() const
{
	return (brain);
}