//
// Created by lune on 8/31/17.
//

#include "Cat.hpp"

Cat::Cat()
{

}

void Cat::outfile(const char *fileName)
{
	std::string str;
	std::ifstream file(fileName);

	getline(file, str, (char)EOF);
	file.close();
	std::cout << str;
}

void Cat::outstd()
{
	std::string str;

	while (1)
	{
		std::cin >> str;
		if (std::cin.eof())
			break ;
		std::cout << str << std::endl;
	}
}