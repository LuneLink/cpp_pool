//
// Created by lune on 8/31/17.
//

#include "Logger.hpp"

int main()
{
	Logger logger("file");

	logger.log("logToFile", "lol1 - file");
	logger.log("logToConsole", "lol1 - console");
	logger.log("logToFile", "lol2 - file");
	logger.log("logToConsole", "lol2 - console");
	logger.log("logToFile", "lol3 - file");
	logger.log("logToConsole", "lol3 - console");
	logger.log("logToFile", "lol4 - file");
	logger.log("logToConsole", "lol4 - console");
	logger.log("logToFile", "lol5 - file");
	logger.log("logToConsole", "lol5 - console");

}
