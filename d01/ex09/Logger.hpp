//
// Created by lune on 8/31/17.
//

#ifndef CPP_POOL_LOGGER_HPP
#define CPP_POOL_LOGGER_HPP

#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>

class Logger
{
	typedef void (Logger::*func)(std::string const &);

private:
	std::ofstream	_logFile;
	std::string 	*_log_names;
	func 			*_loggers;
	size_t			_log_count;

private:
	std::string		makeLogEntry(std::string const &message);
	void			logToConsole(std::string const &message);
	void			logToFile(std::string const &message);

public:
	Logger(const char *fileName);
	~Logger();
	void	log(std::string const &dest, std::string const &message);
};


#endif //CPP_POOL_LOGGER_HPP
