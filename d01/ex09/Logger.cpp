//
// Created by lune on 8/31/17.
//

#include "Logger.hpp"

Logger::Logger(const char *fileName)
{
	_log_count = 2;
	_log_names = new std::string[_log_count];
	_loggers = new func[_log_count];
	_log_names[0] = "logToConsole";
	_log_names[1] = "logToFile";
	_loggers[0] = &Logger::logToConsole;
	_loggers[1] = &Logger::logToFile;
	_logFile.open(fileName, std::ofstream::out | std::ofstream::trunc);
}

std::string		Logger::makeLogEntry(std::string const &message)
{
	time_t				t = time(0);
	struct tm			*now = localtime(&t);
	std::ostringstream	entry;

	entry << '['
		  << (now->tm_year + 1900)
		  << std::setw(2) << std::setfill('0') << now->tm_mon
		  << std::setw(2) << std::setfill('0') << now->tm_mday
		  << '_'
		  << std::setw(2) << std::setfill('0') << now->tm_hour
		  << std::setw(2) << std::setfill('0') << now->tm_min
		  << std::setw(2) << std::setfill('0') << now->tm_sec
		  << "] "
		  << message;
	return (entry.str());
}

void			Logger::logToConsole(std::string const &message)
{
	std::cout << makeLogEntry(message) << std::endl;
}

void			Logger::logToFile(std::string const &message)
{
	if (_logFile.is_open())
		_logFile << makeLogEntry(message) << std::endl;
}

Logger::~Logger()
{
	if (_logFile.is_open())
		_logFile.close();
	delete [] _log_names;
	delete [] _loggers;
}

void	Logger::log(std::string const &dest, std::string const &message)
{
	for(size_t i = 0; i < _log_count; i++)
		if (_log_names[i].compare(dest) == 0)
			(this->*Logger::_loggers[i])(message);
}