//
// Created by Serhii Petrenko on 8/28/17.
//

#ifndef CPP_POOL_HUMANB_HPP
#define CPP_POOL_HUMANB_HPP

#include <string>
#include "Weapon.hpp"
#include "HumanA.hpp"

class HumanB
{
private:
	std::string	_name;
	Weapon		*_weapon;

public:
	HumanB(const char *name);
	void setWeapon(Weapon &weapon);
	void	attack();
};

#endif //CPP_POOL_HUMANB_HPP
