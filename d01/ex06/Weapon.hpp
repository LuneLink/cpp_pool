//
// Created by Serhii Petrenko on 8/28/17.
//

#ifndef CPP_POOL_WEAPON_HPP
#define CPP_POOL_WEAPON_HPP

#include <string>

class Weapon {

private:
	std::string _type;

public:
	Weapon(const char *type);
	const std::string &getType();
	void setType(const char *type);
};


#endif
