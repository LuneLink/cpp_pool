//
// Created by LuneLink on 30.08.2017.
//

#include "Human.hpp"


void			Human::meleeAttack(std::string const &target)
{
    std::cout << _name <<" attack " << target << " with a melee attack" << std::endl;
}

void			Human::rangedAttack(std::string const &target)
{
    std::cout << _name <<" attack " << target << " with a ranged attack" << std::endl;
}

void			Human::intimidatingShout(std::string const &target)
{
    std::cout << _name <<" attack " << target << " with a intimidating shout" << std::endl;
}

Human::Human(const char *name)
{
    this->_name = name;
    _actions_count = 3;
    _action_names = new std::string[_actions_count];
    _actions = new func[_actions_count];
    _action_names[0] = "meleeAttack";
    _action_names[1] = "rangedAttack";
    _action_names[2] = "intimidatingShout";
    _actions[0] = &Human::meleeAttack;
    _actions[1] = &Human::rangedAttack;
    _actions[2] = &Human::intimidatingShout;
}

Human::~Human()
{
    delete [] _action_names;
    delete [] _actions;
}

void Human::action(std::string const &action_name, std::string const &target)
{
    for(size_t i = 0; i < _actions_count; i++)
        if (_action_names[i].compare(action_name) == 0)
            (this->*Human::_actions[i])(target);
}