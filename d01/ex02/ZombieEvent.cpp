//
// Created by lune on 8/25/17.
//

#include "ZombieEvent.hpp"

		ZombieEvent::ZombieEvent(): _type("default_type")
{

}

Zombie *ZombieEvent::newZombie(std::string name)
{
	return (new Zombie(name, _type));
}

void	ZombieEvent::setType(std::string type)
{
	this->_type = type;
}

Zombie	*ZombieEvent::randomChump(void)
{
	std::string name[3] = {"Archi", "Runio", "Branio"};
	Zombie *zombie = newZombie(name[std::rand() % 3]);
	zombie->announce();
	return (zombie);
}