//
// Created by lune on 8/25/17.
//

#ifndef CPP_POOL_ZOMBIEEVENT_HPP
#define CPP_POOL_ZOMBIEEVENT_HPP

#include <string>
#include <iostream>
#include <cstdlib>
#include "Zombie.hpp"

class ZombieEvent {

public:
				ZombieEvent();
	Zombie		*newZombie(std::string name);
	void 		setType(std::string type);
	Zombie		*randomChump(void);

private:
	std::string	_type;
};


#endif
