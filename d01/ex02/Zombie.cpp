//
// Created by lune on 8/25/17.
//

#include "Zombie.hpp"

					Zombie::Zombie()
{

}

					Zombie::Zombie (std::string name, std::string type):
							_name(name), _type(type)
{

}

void				Zombie::setName(std::string name)
{
	this->_name = name;
}

const std::string	&Zombie::getName() const
{
	return (_name);
}

void				Zombie::setType(std::string type)
{
	this->_type = type;
}

const std::string	&Zombie::getType() const
{
	return (_type);
}

void 				Zombie::announce()
{
	std::cout<< "<" << _name << " (" << _type << ")>" << "Braiiiiiiinnnssss..." << std::endl;
}