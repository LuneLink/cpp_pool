
#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <time.h>

int main(void)
{
	ZombieEvent ze;
	ZombieEvent	ze2;
	Zombie		*zombie;
	Zombie		*zombie2;

	srand(time(0));
	ze.setType("Walkers");
	ze2.setType("Runners");
	zombie = ze.newZombie("Archi");
	zombie2 = ze2.newZombie("Carl");

	zombie->announce();
	zombie2->announce();
	delete zombie;
	delete zombie2;

	for(int i = 0; i < 20; i++)
	{

		int rnd = std::rand() % 2;

		if (rnd == 1)
			zombie = ze.randomChump();
		else
			zombie = ze2.randomChump();
		delete zombie;
	}
}